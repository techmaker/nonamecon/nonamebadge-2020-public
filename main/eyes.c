#include "eyes.h"
#include "esp_err.h"
#include "esp_log.h"

static const char *TAG = "EYES";

typedef enum {
	eyes_closed = 0,
	eyes_closed_low,
	eyes_open_low,
	eyes_open,
	eyes_open_clean,
	eyes_open_fully,
	eyes_side,
	eyes_emotion_half,
	eyes_emotion,
	eyes_max,
} eyes_t;

static oled_context_t * left;
static oled_context_t * right;
static bool wakeup = false;

static void wakeup_cb(badge_input_t input, badge_event_t event) {
	if (event == badge_pressed_event) {
		wakeup = true;
	}
}

static inline void drawClear(oled_context_t *ctx) {
	OLED_FillScreen(ctx,													Black);
}

static inline void drawEdge(oled_context_t *ctx) {
	OLED_DrawRect(ctx,		6,		6,		116,	52,						White);
}

static inline void drawClosed(oled_context_t *ctx, uint8_t y, bool bold) {
	OLED_DrawLine(ctx,		16,		y,		111,	y,						White);
	if (bold) {
		OLED_DrawLine(ctx,	16,		y + 1,	111,	y + 1,					White);
	}
}

static inline void drawIris(oled_context_t *ctx, uint8_t x, uint8_t y) {
	OLED_FillRect(ctx,		x + 7,	y + 7,	22,		22,						White);

	OLED_FillTriangle(ctx,	x + 7,	y + 7,	x + 11,	y + 7,	x + 7,	y + 11,	Black);
	OLED_FillTriangle(ctx,	x + 24,	y + 7,	x + 28,	y + 7,	x + 28,	y + 11,	Black);
	OLED_FillTriangle(ctx,	x + 7,	y + 24,	x + 11,	y + 28,	x + 7,	y + 28,	Black);
	OLED_FillTriangle(ctx,	x + 24,	y + 28,	x + 28,	y + 24,	x + 28,	y + 28,	Black);
}

static inline void drawIrisEdge(oled_context_t *ctx, uint8_t x, uint8_t y) {
	OLED_DrawLine(ctx,		x + 11,	y + 0,	x + 24,	y + 0,					White);
	OLED_DrawLine(ctx,		x + 11,	y + 1,	x + 24,	y + 1,					White);
	
	OLED_DrawLine(ctx,		x + 11,	y + 34,	x + 24,	y + 34,					White);
	OLED_DrawLine(ctx,		x + 11,	y + 35,	x + 24,	y + 35,					White);
	
	OLED_DrawLine(ctx,		x + 0,	y + 11,	x + 0,	y + 24,					White);
	OLED_DrawLine(ctx,		x + 1,	y + 11,	x + 1,	y + 24,					White);

	OLED_DrawLine(ctx,		x + 34,	y + 11,	x + 34,	y + 24,					White);
	OLED_DrawLine(ctx,		x + 35,	y + 11,	x + 35,	y + 24,					White);
	
	OLED_DrawLine(ctx,		x + 0,	y + 10,	x + 10,	y + 0,					White);
	OLED_DrawLine(ctx,		x + 1,	y + 10,	x + 10,	y + 1,					White);
	OLED_DrawLine(ctx,		x + 2,	y + 10,	x + 10,	y + 2,					White);
	
	OLED_DrawLine(ctx,		x + 25,	y + 0,	x + 35,	y + 10,					White);
	OLED_DrawLine(ctx,		x + 25,	y + 1,	x + 34,	y + 10,					White);
	OLED_DrawLine(ctx,		x + 25,	y + 2,	x + 33,	y + 10,					White);
	
	OLED_DrawLine(ctx,		x + 0,	y + 25,	x + 10,	y + 35,					White);
	OLED_DrawLine(ctx,		x + 1,	y + 25,	x + 10,	y + 34,					White);
	OLED_DrawLine(ctx,		x + 2,	y + 25,	x + 10,	y + 33,					White);

	OLED_DrawLine(ctx,		x + 25,	y + 35,	x + 35,	y + 25,					White);
	OLED_DrawLine(ctx,		x + 25,	y + 34,	x + 34,	y + 25,					White);
	OLED_DrawLine(ctx,		x + 25,	y + 33,	x + 33,	y + 25,					White);
}

static inline void drawPupilCenter(oled_context_t *ctx, uint8_t x, uint8_t y) {
	OLED_FillRect(ctx,		x + 14,	y + 15,	8,		6,						Black);
	OLED_DrawLine(ctx,		x + 15,	y + 14,	x + 20,	y + 14,					Black);
	OLED_DrawLine(ctx,		x + 15,	y + 21,	x + 20,	y + 21,					Black);
}

static inline void drawPupilLeft(oled_context_t *ctx, uint8_t x, uint8_t y) {
	OLED_FillRect(ctx,		x + 9,	y + 11,	5,		14,						Black);
	OLED_DrawLine(ctx,		x + 12,	y + 25,	x + 14,	y + 25,					Black);
	OLED_DrawLine(ctx,		x + 14,	y + 26,	x + 15,	y + 26,					Black);

	OLED_DrawLine(ctx,		x + 13,	y + 15,	x + 13,	y + 22,					White);
	OLED_DrawPixel(ctx, 	x + 9,	y + 11, 								White);
	OLED_DrawPixel(ctx, 	x + 10,	y + 11, 								White);
	OLED_DrawPixel(ctx, 	x + 9,	y + 12, 								White);
	OLED_DrawPixel(ctx, 	x + 9,	y + 23, 								White);
	OLED_DrawPixel(ctx, 	x + 9,	y + 24, 								White);
	OLED_DrawPixel(ctx, 	x + 10,	y + 24, 								White);
}

static inline void drawPupilRight(oled_context_t *ctx, uint8_t x, uint8_t y) {
	OLED_FillRect(ctx,		x + 22,	y + 11,	5,		14,						Black);
	OLED_DrawLine(ctx,		x + 21,	y + 25,	x + 23,	y + 25,					Black);
	OLED_DrawLine(ctx,		x + 20,	y + 26,	x + 21,	y + 26,					Black);

	OLED_DrawLine(ctx,		x + 22,	y + 15,	x + 22,	y + 22,					White);
	OLED_DrawPixel(ctx, 	x + 25,	y + 11, 								White);
	OLED_DrawPixel(ctx, 	x + 26,	y + 11, 								White);
	OLED_DrawPixel(ctx, 	x + 26,	y + 12, 								White);
	OLED_DrawPixel(ctx, 	x + 26,	y + 23, 								White);
	OLED_DrawPixel(ctx, 	x + 25,	y + 24, 								White);
	OLED_DrawPixel(ctx, 	x + 26,	y + 24, 								White);
}

static inline void drawBrowHrz(oled_context_t *ctx, uint8_t y, bool bold) {
	OLED_FillRect(ctx,		7,		7,		114,	y - 6,					Black);
	OLED_DrawLine(ctx,		7,		y,		120,	y,						White);
	if (bold) {
		OLED_DrawLine(ctx,	7,		y + 1,	120,	y + 1,					White);
	}
}

static inline void drawBrowDiagLeft(oled_context_t *ctx, uint8_t y, uint8_t h, bool bold) {
	OLED_FillRect(ctx,		7,		7,		114,	y - 6,					Black);
	OLED_FillTriangle(ctx,	7,		y, 		120, 	y, 		7,		y + h,	Black);
	OLED_DrawLine(ctx,		7,		y + h,	120,	y,						White);
	if (bold) {
		OLED_DrawLine(ctx,	7,	y + h + 1,	120,	y + 1,				White);
	}
}

static inline void drawBrowDiagRight(oled_context_t *ctx, uint8_t y, uint8_t h, bool bold) {
	OLED_FillRect(ctx,		7,		7,		114,	y - 6,					Black);
	OLED_FillTriangle(ctx,	7,		y, 		120, 	y, 		120,	y + h,	Black);
	OLED_DrawLine(ctx,		7,		y,		120,	y + h,					White);
	if (bold) {
		OLED_DrawLine(ctx,	7,		y + 1,	120,	y + h + 1,				White);
	}
}

static inline void OLED_DrawEye(oled_context_t *ctx, eyes_t eyes_style, bool flip) {
	const uint8_t x = 46, y = 14;
	drawClear(ctx);
	drawEdge(ctx);
	switch(eyes_style) {
	case eyes_closed: 
		drawClosed(ctx, 31, true);
		break;
	case eyes_closed_low: 
		drawClosed(ctx, 45, false);
		break;
	case eyes_open_low:
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		drawPupilCenter(ctx, x, y);
		drawBrowHrz(ctx, 33, false);
		break;
	case eyes_open: 
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		drawPupilCenter(ctx, x, y);
		drawBrowHrz(ctx, 20, false);
		break;
	case eyes_open_clean: 
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		drawPupilCenter(ctx, x, y);
		break;
	case eyes_open_fully:
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		break;
	case eyes_side: 
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		flip ? drawPupilRight(ctx, x, y) : drawPupilLeft(ctx, x, y);
		break;
	case eyes_emotion_half:  
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		drawPupilCenter(ctx, x, y);
		flip ? drawBrowDiagRight(ctx, 14, 14, false) : drawBrowDiagLeft(ctx, 14, 14, false);
		break;
	case eyes_emotion:   
		drawIris(ctx, x, y);
		drawIrisEdge(ctx, x, y);
		drawPupilCenter(ctx, x, y);
		flip ? drawBrowDiagRight(ctx, 12, 26, true) : drawBrowDiagLeft(ctx, 12, 26, true);
		break;
	case eyes_max: 
	default:
		break;
	}
}

void Eyes_Init(oled_context_t * ctx_left, oled_context_t * ctx_right, badge_input_t wakeup_input) {
	left = ctx_left;
	right = ctx_right;
	badge_register_event_callback(wakeup_input, wakeup_cb);
}

void Eyes_StateClosed() {
	OLED_DrawEye(left, eyes_closed, false);
	OLED_DrawEye(right, eyes_closed, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateClosedLow() {
	OLED_DrawEye(left, eyes_closed_low, false);
	OLED_DrawEye(right, eyes_closed_low, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateBrow() {
	OLED_DrawEye(left, eyes_open, false);
	OLED_DrawEye(right, eyes_open, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateBrowLow() {
	OLED_DrawEye(left, eyes_open_low, false);
	OLED_DrawEye(right, eyes_open_low, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateOpened() {
	OLED_DrawEye(left, eyes_open_clean, false);
	OLED_DrawEye(right, eyes_open_clean, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateWinked() {
	OLED_DrawEye(left, eyes_open, false);
	OLED_DrawEye(right, eyes_closed, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateDope() {
	OLED_DrawEye(left, eyes_open_fully, false);
	OLED_DrawEye(right, eyes_open_fully, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateLeft() {
	OLED_DrawEye(left, eyes_side, false);
	OLED_DrawEye(right, eyes_side, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateRight() {
	OLED_DrawEye(left, eyes_side, true);
	OLED_DrawEye(right, eyes_side, true);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateOutside() {
	OLED_DrawEye(left, eyes_side, false);
	OLED_DrawEye(right, eyes_side, true);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateInside() {
	OLED_DrawEye(left, eyes_side, true);
	OLED_DrawEye(right, eyes_side, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateAngryHalf() {
	OLED_DrawEye(left, eyes_emotion_half, true);
	OLED_DrawEye(right, eyes_emotion_half, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateAngry() {
	OLED_DrawEye(left, eyes_emotion, true);
	OLED_DrawEye(right, eyes_emotion, false);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateSadHalf() {
	OLED_DrawEye(left, eyes_emotion_half, false);
	OLED_DrawEye(right, eyes_emotion_half, true);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}

void Eyes_StateSad() {
	OLED_DrawEye(left, eyes_emotion, false);
	OLED_DrawEye(right, eyes_emotion, true);
	OLED_UpdateScreen(left);
	OLED_UpdateScreen(right);
}


void Eyes_Sleep() {
	wakeup = false;
	ESP_LOGI(TAG, "Sleeping, to wake touch the right chip");
	Eyes_StateClosed();
}

void Eyes_Wake() {
	uint32_t ms = 100;
	while(!wakeup) {
		vTaskDelay(ms / portTICK_RATE_MS);
	}
	Eyes_StateBrowLow();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateBrow();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
}

void Eyes_Blink() {
	uint32_t ms = 30;
	vTaskDelay((1500 + rand() % 3000) / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateBrow();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateBrowLow();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateClosedLow();
	vTaskDelay(ms * 2 / portTICK_RATE_MS);
	Eyes_StateBrowLow();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateBrow();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay((1500 + rand() % 3000) / portTICK_RATE_MS);
}

void Eyes_Wonder() {
	uint32_t ms = 250;
	Eyes_StateOpened();
	vTaskDelay(ms * 2 / portTICK_RATE_MS);
	Eyes_StateBrow();
	vTaskDelay(ms * 4 / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateLeft();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateRight();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateLeft();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateRight();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
}

void Eyes_Dummy() {
	uint32_t ms = 500;
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateBrow();
	vTaskDelay(ms * 2 / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms * 2 / portTICK_RATE_MS);
	Eyes_StateInside();
	vTaskDelay(ms * 4 / portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateOutside();
	vTaskDelay(ms * 4/ portTICK_RATE_MS);
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
}


void Eyes_Sad() {
	uint32_t ms = 75;
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateSadHalf();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateSad();
	vTaskDelay(ms * 30 / portTICK_RATE_MS);
}

void Eyes_Angry() {
	uint32_t ms = 75;
	Eyes_StateOpened();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateAngryHalf();
	vTaskDelay(ms / portTICK_RATE_MS);
	Eyes_StateAngry();
	vTaskDelay(ms * 30 / portTICK_RATE_MS);
}