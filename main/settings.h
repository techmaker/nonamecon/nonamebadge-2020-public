#pragma once

#include <ctype.h>
#include "esp_err.h"

#define SETSNAMESPACE "badge"

#define LED_BRIGHT "led-brig"

esp_err_t settings_setU32(const char *name, uint32_t value);
esp_err_t settings_getU32(const char *name, uint32_t *value);
esp_err_t settings_setI32(const char *name, int32_t value);
esp_err_t settings_getI32(const char *name, int32_t *value);
esp_err_t settings_setString(const char *name, const char *value);
esp_err_t settings_getString(const char *name, char *value, size_t *maxLen);
esp_err_t settings_setBlob(const char *name, const void *value, size_t length);
esp_err_t settings_getBlob(const char *name, void *value, size_t *maxLen);