#include <limits.h>

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "esp_bt.h"
#include "esp_log.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"

#include "esp_event.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"
#include "esp_ota_ops.h"
#include "esp_wifi.h"

#include "sdkconfig.h"

// #define CONFIG_APP_PROJECT_VER "2020.01"
#define NNC_FW_VERSION "2020.01"

#define PREPARE_BUF_MAX_SIZE (1024)

void taskWiFi(void *pvParameters);
void taskBLE(void *pvParameters);
void taskLEDsnBTN(void *pvParameters);

_Noreturn
void taskBadgeOS(void *pvParameters);
void taskOLED(void *pvParameters);
void taskInputs(void *pvParameters);

void taskMitm(void *pvParameters);

extern EventGroupHandle_t wifi_event_group;