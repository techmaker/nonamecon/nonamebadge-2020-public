#include "bsp.h"
#include "driver/adc.h"
#include "esp_bt.h"
#include "esp_log.h"
#include "esp_sleep.h"
#include "esp_wifi.h"
#include "freertos/task.h"
#include "ibat.h"
#include "mqtt_config.h"

extern void badge_turnOffOLED();

extern void badge_turnOnOLED();

void taskIBAT(void *pvParameters) {
	while (1) {
		uint32_t mV = ibat_get_mV(bsp_config.pins.vbat);

		if (mV < 3400 && mV > 2200) {
			// battery low
			// display off
			badge_turnOffOLED();
			adc_power_off();
			esp_wifi_stop();
			esp_bt_controller_disable();
			esp_deep_sleep(1000000LL * 60);
		} else {
			vTaskDelay(30000 / portTICK_PERIOD_MS);
		}
	}
}