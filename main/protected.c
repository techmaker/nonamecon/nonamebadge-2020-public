#include "protected.h"

#include <stdlib.h>
#include <strings.h>

#include "obfuscator.h"

static char *base_server_url = 0;
static char *root_pass = 0;
static char *ota_url = 0;
static char *ota_version_url = 0;

inline int _len(const char *s) {
	for (size_t i = 0; i < 100; i++) {
		if (0x00 == s[i]) return i;
	}
	return 0;
}

bool protected_isEqual(const char *s, char *protected_s) {
	int l1 = _len(s);
	int l2 = _len(protected_s);
	int result = protected_cmp(s, protected_s, _len(s));
	free(protected_s);
	return result == 0 && l1 == l2;
}