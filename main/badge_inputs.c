#include "badge_inputs.h"

#include <esp_log.h>

#include "bsp.h"
#include "driver/touch_pad.h"
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"
#include "freertos/task.h"
#include "pca9539.h"

#define TOUCH_PAD_NO_CHANGE (-1)
#define TOUCH_THRESH_NO_USE (0)
#define TOUCH_FILTER_MODE_EN (1)
#define TOUCH_FILTER_PERIOD (10)
#define TOUCH_THRESH_PRESSED (500)

#define CALLBACK_POOL_SIZE 64

static const char *TAG = "BADGE_INPUTS";

char const *const input_name[] = {
    [input_none] = "none",

    [input_button_boot] = "button_boot",

    [input_chip_left] = "chip_left",
    [input_chip_right] = "chip_right",

    [input_arrow_up] = "arrow_up",
    [input_arrow_down] = "arrow_down",
    [input_arrow_left] = "arrow_left",
    [input_arrow_right] = "arrow_right",

    [input_arrow_left_up] = "arrow_left_up",
    [input_arrow_left_down] = "arrow_left_down",
    [input_arrow_right_up] = "arrow_right_up",
    [input_arrow_right_down] = "arrow_right_down",

    [input_left_dpad_left] = "left_dpad_left",
    [input_left_dpad_right] = "left_dpad_right",
    [input_left_dpad_up] = "left_dpad_up",
    [input_left_dpad_down] = "left_dpad_down",
    [input_left_dpad_center] = "left_dpad_center",

    [input_right_dpad_left] = "right_dpad_left",
    [input_right_dpad_right] = "right_dpad_right",
    [input_right_dpad_up] = "right_dpad_up",
    [input_right_dpad_down] = "right_dpad_down",
    [input_right_dpad_center] = "right_dpad_center",
};

typedef enum {
	dpad_left = 0,
	dpad_right,
	dpad_up,
	dpad_down,
	dpad_center,
} dpad_pins_t;

static pca9539_context_t dpad;
static const pca9539_port_t dpad_left_port = PCA9539_PORT_0;
static const pca9539_pin_t dpad_left_pins[] = {PCA9539_PIN_0,   // left
                                               PCA9539_PIN_2,   // right
                                               PCA9539_PIN_4,   // up
                                               PCA9539_PIN_1,   // down
                                               PCA9539_PIN_3};  // center
static const pca9539_pin_t dpad_left_pinmask = PCA9539_PIN_0 | PCA9539_PIN_2 | PCA9539_PIN_4 | PCA9539_PIN_1 | PCA9539_PIN_3;
static const pca9539_port_t dpad_right_port = PCA9539_PORT_1;
static const pca9539_pin_t dpad_right_pins[] = {PCA9539_PIN_2,   // left
                                                PCA9539_PIN_3,   // right
                                                PCA9539_PIN_0,   // up
                                                PCA9539_PIN_4,   // down
                                                PCA9539_PIN_1};  // center
static const pca9539_pin_t dpad_right_pinmask = PCA9539_PIN_2 | PCA9539_PIN_3 | PCA9539_PIN_0 | PCA9539_PIN_4 | PCA9539_PIN_1;
static SemaphoreHandle_t dpad_semaphore = NULL;
static uint8_t dpad_state[2] = {0};

typedef struct {
	badge_event_callback_t callback_function;
	badge_input_t input;
} event_callback_t;

static event_callback_t registered_callbacks[CALLBACK_POOL_SIZE] = {0};

static void initButtons(void);
static void initTouchPads(void);
static void initDPads(i2c_drv_t *i2c_drv);
static void checkDPads(void);
static void callbackDPad(void *arg);
static inline badge_event_t getTouchState(touch_pad_t pad);
static inline badge_event_t getDPadState(pca9539_port_t port, pca9539_pin_t pin);
static badge_event_t getInputState(badge_input_t input);
static void callEventCallbacks(badge_input_t input, badge_event_t event);

esp_err_t badge_register_event_callback(badge_input_t input, badge_event_callback_t callback_function) {
	for (size_t i = 0; i < CALLBACK_POOL_SIZE; i++) {
		if (registered_callbacks[i].callback_function == callback_function && registered_callbacks[i].input == input) {
			return ESP_OK;
		}
		if (registered_callbacks[i].callback_function == NULL) {
			registered_callbacks[i].callback_function = callback_function;
			registered_callbacks[i].input = input;
			return ESP_OK;
		}
	}

	return ESP_FAIL;
}

esp_err_t badge_unregister_event_callback(badge_input_t input, badge_event_callback_t callback_function) {
	for (size_t i = 0; i < CALLBACK_POOL_SIZE; i++) {
		if (registered_callbacks[i].callback_function == callback_function &&
		    registered_callbacks[i].input == input) {
			registered_callbacks[i].callback_function = NULL;
			registered_callbacks[i].input = input_none;
			return ESP_OK;
		}
	}

	return ESP_FAIL;
}

void badge_input_test(badge_input_t input, badge_event_t event) {
	ESP_LOGI(TAG, "%s %s", input_name[input], event == badge_pressed_event ? "pressed" : "released");
}

void taskInputs(void *pvParameters) {
	uint32_t lastPressedTime[32] = {0};
	i2c_drv_t *i2c_drv = (i2c_drv_t *)pvParameters;

	initButtons();
	badge_register_event_callback(input_none, badge_input_test);
	badge_register_event_callback(input_button_boot, badge_input_test);

	initTouchPads();
	if (bsp_is_red()) {
		badge_register_event_callback(input_chip_left, badge_input_test);
		badge_register_event_callback(input_chip_right, badge_input_test);
		badge_register_event_callback(input_arrow_left_up, badge_input_test);
		badge_register_event_callback(input_arrow_left_down, badge_input_test);
		badge_register_event_callback(input_arrow_right_up, badge_input_test);
		badge_register_event_callback(input_arrow_right_down, badge_input_test);
	} else {
		// Black Badge V2.1 doesn't have left chip as touchpad
		badge_register_event_callback(input_chip_right, badge_input_test);
		badge_register_event_callback(input_arrow_up, badge_input_test);
		badge_register_event_callback(input_arrow_down, badge_input_test);
		badge_register_event_callback(input_arrow_left, badge_input_test);
		badge_register_event_callback(input_arrow_right, badge_input_test);
	}

	if (bsp_has_joystick()) {
		initDPads(i2c_drv);
		badge_register_event_callback(input_left_dpad_left, badge_input_test);
		badge_register_event_callback(input_left_dpad_right, badge_input_test);
		badge_register_event_callback(input_left_dpad_up, badge_input_test);
		badge_register_event_callback(input_left_dpad_down, badge_input_test);
		badge_register_event_callback(input_left_dpad_center, badge_input_test);
		badge_register_event_callback(input_right_dpad_left, badge_input_test);
		badge_register_event_callback(input_right_dpad_right, badge_input_test);
		badge_register_event_callback(input_right_dpad_up, badge_input_test);
		badge_register_event_callback(input_right_dpad_down, badge_input_test);
		badge_register_event_callback(input_right_dpad_center, badge_input_test);
	}

	while (1) {
		if (bsp_has_joystick()) {
			checkDPads();
		}
		for (size_t i = 0; i < 32; i++) {
			if (getInputState(i) == badge_pressed_event) {
				if (lastPressedTime[i] == 0) {
					callEventCallbacks(i, badge_pressed_event);
				}
				lastPressedTime[i] = esp_timer_get_time();
			} else {
				if (lastPressedTime[i] != 0) {
					callEventCallbacks(i, badge_released_event);
				}
				lastPressedTime[i] = 0;
			}
		}
		vTaskDelay(1);
		gpio_set_direction(12, GPIO_MODE_INPUT);
		gpio_set_direction(13, GPIO_MODE_INPUT);
		gpio_set_direction(14, GPIO_MODE_INPUT);
		gpio_set_direction(15, GPIO_MODE_INPUT);
	}
	vTaskDelete(NULL);
}

static void initButtons(void) {
	gpio_config_t io_conf;
	/* Reset pin */
	io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_OUTPUT;
	io_conf.pin_bit_mask = 1ULL << bsp_config.pins.boot;
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
	io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&io_conf);
}
static inline badge_event_t getButtonState(gpio_num_t pin) {
	return gpio_get_level(pin) ? badge_released_event : badge_pressed_event;
}

static void initTouchPads(void) {
	gpio_config_t io_conf;
	/* IO14 has to be disabled */
	io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_INPUT;
	io_conf.pin_bit_mask = 1ULL << bsp_config.pins.jtag[0] |
	                       1ULL << bsp_config.pins.jtag[1] |
	                       1ULL << bsp_config.pins.jtag[2] |
	                       1ULL << bsp_config.pins.jtag[3];
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
	io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&io_conf);

	touch_pad_init();
	touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
	for (uint8_t i = 0; i < 6; i++) {
		if (bsp_config.pins.touch[i] < TOUCH_PAD_MAX) {
			touch_pad_config(bsp_config.pins.touch[i], TOUCH_THRESH_NO_USE);
		}
	}
	touch_pad_filter_start(TOUCH_FILTER_PERIOD);
}

static inline badge_event_t getTouchState(touch_pad_t pad) {
	uint16_t touch_filter_value;
	if (pad >= TOUCH_PAD_MAX) {
		return badge_released_event;
	}
	touch_pad_read_filtered(pad, &touch_filter_value);
	if (touch_filter_value < TOUCH_THRESH_PRESSED) {
		return badge_pressed_event;
	} else {
		return badge_released_event;
	}
}

static void initDPads(i2c_drv_t *i2c_drv) {
	dpad.i2c_drv = i2c_drv;
	dpad.i2c_addr = PCA9539_I2C_ADDRESS1;
	dpad.rst_pin = bsp_config.pins.io_rst;
	dpad.int_pin = bsp_config.pins.io_int;
	dpad.isr_handler = (gpio_isr_t)callbackDPad;
	dpad_semaphore = xSemaphoreCreateBinary();
	pca9539_init(&dpad);
	pca9539_config_port(&dpad,
	                    dpad_left_port,
	                    dpad_left_pinmask,
	                    PCA9539_INPUT,
	                    PCA9539_INVERTED);
	pca9539_config_port(&dpad,
	                    dpad_left_port,
	                    PCA9539_PIN_ALL ^ dpad_left_pinmask,
	                    PCA9539_OUTPUT,
	                    PCA9539_NORMAL);
	pca9539_write_port(&dpad, dpad_left_port, 0x00);
	pca9539_config_port(&dpad,
	                    dpad_right_port,
	                    dpad_right_pinmask,
	                    PCA9539_INPUT,
	                    PCA9539_INVERTED);
	pca9539_config_port(&dpad,
	                    dpad_right_port,
	                    PCA9539_PIN_ALL ^ dpad_right_pinmask,
	                    PCA9539_OUTPUT,
	                    PCA9539_NORMAL);
	pca9539_write_port(&dpad, dpad_right_port, 0x00);
	pca9539_int_enable(&dpad);
}

static void checkDPads() {
	if (xSemaphoreTake(dpad_semaphore, (TickType_t)0) == pdTRUE) {
		*((uint16_t *)dpad_state) = pca9539_read_all(&dpad);
	}
}

static void callbackDPad(void *arg) {
	static BaseType_t xHigherPriorityTaskWoken;
	xSemaphoreGiveFromISR(dpad_semaphore, &xHigherPriorityTaskWoken);
}

static inline badge_event_t getDPadState(pca9539_port_t port, pca9539_pin_t pin) {
	return (dpad_state[port] & pin) == pin ? badge_pressed_event : badge_released_event;
}

static badge_event_t getInputState(badge_input_t input) {
	switch (input) {
	case input_button_boot:
		return getButtonState(bsp_config.pins.boot);
		break;
	case input_chip_left:
		return getTouchState(bsp_config.pins.touch[0]);
		break;
	case input_chip_right:
		return getTouchState(bsp_config.pins.touch[1]);
		break;
	case input_arrow_up:
	case input_arrow_left_up:
		return getTouchState(bsp_config.pins.touch[2]);
		break;
	case input_arrow_down:
	case input_arrow_left_down:
		return getTouchState(bsp_config.pins.touch[3]);
		break;
	case input_arrow_left:
	case input_arrow_right_up:
		return getTouchState(bsp_config.pins.touch[4]);
		break;
	case input_arrow_right:
	case input_arrow_right_down:
		return getTouchState(bsp_config.pins.touch[5]);
		break;
	case input_left_dpad_left:
		return getDPadState(dpad_left_port, dpad_left_pins[dpad_left]);
		break;
	case input_left_dpad_right:
		return getDPadState(dpad_left_port, dpad_left_pins[dpad_right]);
		break;
	case input_left_dpad_up:
		return getDPadState(dpad_left_port, dpad_left_pins[dpad_up]);
		break;
	case input_left_dpad_down:
		return getDPadState(dpad_left_port, dpad_left_pins[dpad_down]);
		break;
	case input_left_dpad_center:
		return getDPadState(dpad_left_port, dpad_left_pins[dpad_center]);
		break;
	case input_right_dpad_left:
		return getDPadState(dpad_right_port, dpad_right_pins[dpad_left]);
		break;
	case input_right_dpad_right:
		return getDPadState(dpad_right_port, dpad_right_pins[dpad_right]);
		break;
	case input_right_dpad_up:
		return getDPadState(dpad_right_port, dpad_right_pins[dpad_up]);
		break;
	case input_right_dpad_down:
		return getDPadState(dpad_right_port, dpad_right_pins[dpad_down]);
		break;
	case input_right_dpad_center:
		return getDPadState(dpad_right_port, dpad_right_pins[dpad_center]);
		break;
	default:
		return badge_released_event;
		break;
	}
}

static void callEventCallbacks(badge_input_t input, badge_event_t event) {
	for (size_t i = 0; i < CALLBACK_POOL_SIZE; i++) {
		if (registered_callbacks[i].input == input) {
			registered_callbacks[i].callback_function(input, event);
		}
	}
}