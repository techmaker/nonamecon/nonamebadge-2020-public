#include "app.h"

#include <string.h>

#include "bsp.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include "sensors.h"
#include "settings.h"

static void init_context() {
	// Initialize NVS.
	esp_err_t ret = nvs_flash_init();
	if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
	    ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		ret = nvs_flash_init();
	}
	ESP_ERROR_CHECK(ret);
}

static void gpio_test() {
	int cnt = 0;
	while (cnt < 20) {
		gpio_set_level(23, ++cnt % 2);
		vTaskDelay(100 / portTICK_RATE_MS);
	}
}

void app_main() {
	init_context();
	bsp_init();

	xTaskCreate(&taskLEDsnBTN, "", 1024 * 4, NULL, 5, NULL);

	if (bsp_has_display()) {
		xTaskCreate(&taskOLED, "", 1024 * 4, bsp_config.i2c_drv_oled, 5, NULL);
	}

	if (bsp_has_sensors()) {
		xTaskCreate(&taskSensors, "", 1024 * 4, bsp_config.i2c_drv_main, 5, NULL);
	}

	if (bsp_has_joystick()) {
		xTaskCreate(&taskInputs, "", 1024 * 4, bsp_config.i2c_drv_main, 5, NULL);
	} else {
		xTaskCreate(&taskInputs, "", 1024 * 4, NULL, 5, NULL);
	}

	gpio_test();
}