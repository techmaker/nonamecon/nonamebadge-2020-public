#include "leds.h"

#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <math.h>

#include "badge_inputs.h"
#include "bsp.h"
#include "sensors.h"
#include "settings.h"
#include "ws2812_control.h"

static int brightnessPerc = 50;
static uint32_t cachedColors[LED_NUM] = {0};
static TaskHandle_t taskLedHandle;

typedef enum {
	LED_OFF = 0,
	LED_RAINBOW = 1,
	LED_IAQ = 2,
	LED_STATE_COUNT
} led_task_state_t;

static led_task_state_t task_state = LED_RAINBOW;

static void _led_clear() {
	for (int i = 0; i < LED_NUM; i++) {
		led_setColor(i, 0, 0);
	}
}

void led_setBrightness(int perc) {
	if (perc < 0)
		perc = 0;
	if (perc > 100)
		perc = 100;
	brightnessPerc = perc;

	for (size_t i = 0; i < LED_NUM; i++) {
		led_setColor(i, cachedColors[i], 0);
	}
	led_updateAll();

	settings_setI32(LED_BRIGHT, brightnessPerc);
}

static led_state_t nnc_leds;

static void _led_internal_init() {
	gpio_config_t io_conf;
	// disable interrupt
	io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
	// set as output mode
	io_conf.mode = GPIO_MODE_OUTPUT;
	// bit mask of the pins that you want to set,e.g.GPIO18/19
	io_conf.pin_bit_mask = 1ULL << bsp_config.pins.led;
	// disable pull-down mode
	io_conf.pull_down_en = 0;
	// disable pull-up mode
	io_conf.pull_up_en = 0;
	// configure GPIO with the given settings
	gpio_config(&io_conf);
}

static void _led_init() {
	_led_internal_init();
	ws2812_control_init();
	_led_clear();
	led_updateAll();
}

static uint32_t _led_color(uint8_t r, uint8_t g, uint8_t b) {
	return ((uint32_t)r << 8) | ((uint32_t)g << 16) | b;
}

void led_setSymmetricColor(int ledIndex, uint32_t color, int customBrightness) {
	ledIndex %= 9;
	led_setColor(ledIndex, color, customBrightness);
	led_setColor(17 - ledIndex, color, customBrightness);
}

void led_setColor(int ledIndex, uint32_t color, int customBrightness) {
	float brightPerc = brightnessPerc / 100.0;

	if (customBrightness) {
		brightPerc = customBrightness / 100.0;
	}

	cachedColors[ledIndex] = color;

	uint8_t r = (color >> 16) & 0xFF;
	uint8_t g = (color >> 8) & 0xFF;
	uint8_t b = (color)&0xFF;

	r *= brightPerc;
	g *= brightPerc;
	b *= brightPerc;

	nnc_leds.leds[ledIndex] = _led_color(r, g, b);
}

void led_updateAll() {
	ws2812_write_leds(&nnc_leds);
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t led_wheel(uint8_t WheelPos) {
	if (WheelPos < 85) {
		return _led_color(WheelPos * 3, 255 - WheelPos * 3, 0);
	} else if (WheelPos < 170) {
		WheelPos -= 85;
		return _led_color(255 - WheelPos * 3, 0, WheelPos * 3);
	} else {
		WheelPos -= 170;
		return _led_color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
}

static uint32_t btnClickCount = 0;

void btnTechMaker_clbk(badge_input_t input, badge_event_t event) {
	if (event == badge_pressed_event) {
		btnClickCount++;
	}
}

void btnUpBright_clbk(badge_input_t input, badge_event_t event) {
	if (event == badge_pressed_event) {
		brightnessPerc += 5;

		if (brightnessPerc > 80) {
			brightnessPerc = 80;
		}
	}
}

void btnDownBright_clbk(badge_input_t input, badge_event_t event) {
	if (event == badge_pressed_event) {
		brightnessPerc -= 5;

		if (brightnessPerc < 5) {
			brightnessPerc = 5;
		}
	}
}

void btnUpState_clbk(badge_input_t input, badge_event_t event) {
	if (event == badge_pressed_event) {
		task_state = (task_state + 1) % LED_STATE_COUNT;
		xTaskNotifyGive(taskLedHandle);
	}
}

void btnDownState_clbk(badge_input_t input, badge_event_t event) {
	if (event == badge_pressed_event) {
		task_state = (task_state - 1);
		if ((int)task_state < 0) {
			task_state = LED_STATE_COUNT - 1;
		}
		xTaskNotifyGive(taskLedHandle);
	}
}

static void _led_wait_input(int ms) {
	ulTaskNotifyTake(pdFALSE, ms / portTICK_PERIOD_MS);
}

static void _led_gen_effect(uint32_t iaq_color) {
	static uint32_t period = 0;
	int customBrightness = 0;

	if ((period / 10) % 2 == 0 && nnc_leds.blink_effect) {
		iaq_color = 0;
	}

	if (nnc_leds.breath_effect) {
		customBrightness = ((period / 5) % (brightnessPerc * 2));

		customBrightness -= brightnessPerc;
		if (customBrightness < 0) {
			customBrightness = -customBrightness;
		}

		if (customBrightness == 0) {
			iaq_color = 0;
		}
	}

	for (int i = 0; i < LED_NUM; i++) {
		if (nnc_leds.only_cross &&
		    ((i < 9 && i % 2 == 1) || (i >= 9 && i % 2 == 0)) &&
		    iaq_color != 0) {
			continue;
		}
		led_setColor(i, iaq_color, customBrightness);
	}

	led_updateAll();

	period++;
}

static uint32_t _led_getSensorColor() {
	sensors_data_t sensors_data;
	sensors_getValues(&sensors_data);
	nnc_leds.only_cross = false;
	nnc_leds.blink_effect = false;
	nnc_leds.breath_effect = false;

	if (sensors_data.iaq <= 50.0) {
		// excellent
		return GREEN;
	}

	if (sensors_data.iaq <= 100.0) {
		// good
		return 0x80FF00;
	}

	if (sensors_data.iaq <= 150.0) {
		// Lightly polluted
		return 0xFFFF00;
	}

	if (sensors_data.iaq <= 200.0) {
		// Moderately polluted
		return 0xFF8000;
	}

	if (sensors_data.iaq <= 250.0) {
		// Heavely polluted
		nnc_leds.blink_effect = true;
		return 0xFF3333;
	}

	if (sensors_data.iaq <= 350.0) {
		// Severily polluted
		nnc_leds.blink_effect = true;
		return 0x660000;
	}

	nnc_leds.blink_effect = true;
	nnc_leds.breath_effect = false;
	nnc_leds.only_cross = true;
	return RED;
}

extern void badge_turnOffOLED();

extern void badge_turnOnOLED();

void taskLEDsnBTN(void *pvParameters) {
	taskLedHandle = xTaskGetCurrentTaskHandle();

	esp_err_t err = settings_getI32(LED_BRIGHT, &brightnessPerc);
	if (err != ESP_OK) {
		// set brightness to 10% by default
		brightnessPerc = 10;
	}

	_led_init();

	uint16_t i = 0, rainbowRound = 0;

	nnc_leds.blink_effect = false;
	nnc_leds.breath_effect = false;
	nnc_leds.only_cross = false;

	badge_register_event_callback(input_chip_right, btnTechMaker_clbk);

	badge_register_event_callback(input_arrow_up, btnUpBright_clbk);
	badge_register_event_callback(input_left_dpad_up, btnUpBright_clbk);
	badge_register_event_callback(input_right_dpad_up, btnUpBright_clbk);

	badge_register_event_callback(input_arrow_right, btnUpState_clbk);
	badge_register_event_callback(input_left_dpad_right, btnUpState_clbk);
	badge_register_event_callback(input_right_dpad_right, btnUpState_clbk);

	badge_register_event_callback(input_arrow_down, btnDownBright_clbk);
	badge_register_event_callback(input_left_dpad_down, btnDownBright_clbk);
	badge_register_event_callback(input_right_dpad_down, btnDownBright_clbk);

	badge_register_event_callback(input_arrow_left, btnDownState_clbk);
	badge_register_event_callback(input_left_dpad_left, btnDownState_clbk);
	badge_register_event_callback(input_right_dpad_left, btnDownState_clbk);

	while (1) {
		switch (task_state) {
		case LED_RAINBOW: {
			rainbowRound++;
			for (i = 0; i < LED_NUM; i++) {
				led_setColor(i, led_wheel((rainbowRound + i * 22) & 0xFF), 0);
			}
			led_updateAll();
			_led_wait_input(10);
			break;
		}
		case LED_IAQ: {
			// no need for Hysteresis, its already inside the sensor :)
			uint32_t iaq_color = _led_getSensorColor();
			_led_gen_effect(iaq_color);
			_led_wait_input(10);
			break;
		}
		case LED_OFF:
		default:
			_led_clear();
			led_updateAll();
			badge_turnOffOLED();
			_led_wait_input(10);
			if (task_state != LED_OFF){
                badge_turnOnOLED();
			}
			break;
		}
	}
}