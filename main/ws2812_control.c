#include "ws2812_control.h"

#include "driver/rmt.h"

// This is the buffer which the hw peripheral will access while pulsing the
// output pin
rmt_item32_t led_data_buffer[LED_BUFFER_ITEMS];

static void setup_rmt_data_buffer(led_state_t* new_state);

void ws2812_control_init(void) {
	rmt_config_t config = RMT_DEFAULT_CONFIG_TX(bsp_config.pins.ws2812, LED_RMT_TX_CHANNEL);
	config.tx_config.loop_en = true;
	config.tx_config.carrier_en = false;
	config.tx_config.idle_output_en = true;
	config.tx_config.idle_level = 0;
	config.clk_div = 2;

	ESP_ERROR_CHECK(rmt_config(&config));
	ESP_ERROR_CHECK(rmt_driver_install(config.channel, 0, 0));
	// rmt_translator_init((rmt_channel_t)config->dev, ws2812_rmt_adapter);
}

void ws2812_write_leds(led_state_t* new_state) {
	setup_rmt_data_buffer(new_state);
	ESP_ERROR_CHECK(rmt_write_items(LED_RMT_TX_CHANNEL, led_data_buffer, LED_BUFFER_ITEMS, true));
	gpio_set_direction(14, GPIO_MODE_INPUT);
	gpio_set_direction(15, GPIO_MODE_INPUT);
	ESP_ERROR_CHECK(rmt_wait_tx_done(LED_RMT_TX_CHANNEL, portMAX_DELAY));
}

static void IRAM_ATTR setup_rmt_data_buffer(led_state_t* new_state) {
	const rmt_item32_t bit1 = (rmt_item32_t){{{WS2812_T1H_NS, 1, WS2812_T1L_NS, 0}}};
	const rmt_item32_t bit0 = (rmt_item32_t){{{WS2812_T0H_NS, 1, WS2812_T0L_NS, 0}}};
	const rmt_item32_t bitReset = (rmt_item32_t){{{WS2812_RESET_US, 0, WS2812_RESET_US, 0}}};
	gpio_set_direction(14, GPIO_MODE_INPUT);
	gpio_set_direction(12, GPIO_MODE_INPUT);
	for (uint32_t led = 0; led < LED_NUM; led++) {
		uint32_t bits_to_send = new_state->leds[led];
		uint32_t mask = 1 << (BITS_PER_LED_CMD - 1);
		for (uint32_t bit = 0; bit < BITS_PER_LED_CMD; bit++) {
			uint32_t bit_is_set = bits_to_send & mask;
			led_data_buffer[led * BITS_PER_LED_CMD + bit] =
			    bit_is_set ? bit1
			               : bit0;
			mask >>= 1;
		}
	}
	led_data_buffer[LED_BUFFER_ITEMS - 1] = bitReset;
}
