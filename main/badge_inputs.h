#pragma once
#include "esp_err.h"

typedef enum {
    input_none,

	input_button_boot,

    input_chip_left,
	input_chip_right,
	// black badge arrows
    input_arrow_up,
    input_arrow_down,
    input_arrow_left,
    input_arrow_right,
	// red badge arrows
	input_arrow_left_up,
    input_arrow_left_down,
	input_arrow_right_up,
    input_arrow_right_down,

    input_left_dpad_left,
    input_left_dpad_right,
    input_left_dpad_up,
    input_left_dpad_down,
    input_left_dpad_center,
    
    input_right_dpad_left,
    input_right_dpad_right,
    input_right_dpad_up,
    input_right_dpad_down,
    input_right_dpad_center,
} badge_input_t;

typedef enum {
    badge_pressed_event = 0,
    badge_released_event
} badge_event_t;

typedef void (*badge_event_callback_t)(badge_input_t, badge_event_t);

esp_err_t badge_register_event_callback(badge_input_t input, badge_event_callback_t callback_function);
esp_err_t badge_unregister_event_callback(badge_input_t input, badge_event_callback_t callback_function);