#pragma once

#define ACK_CHECK_EN 0x1   // I2C master will check ack from slave
#define ACK_CHECK_DIS 0x0  // I2C master will not check ack from slave
#define ACK 0x0            // I2C ack value
#define NACK 0x1           // I2C nack value

typedef struct
{
	float temperature;
	float humidity;
	float pressure;
	float iaq;
} sensors_data_t;

void taskSensors(void *params);
void sensors_getValues(sensors_data_t *values);