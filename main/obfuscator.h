#pragma once

#define fputs_secure(s) { char *msg = s; fputs(msg, stdout); free(msg); }
#define fputs_secure_nl(s) { char *msg = s; fputs(msg, stdout); fputs("\r\n", stdout); free(msg); }

char *protect(char *s);
char *deobfuscate(char *buffer, int len);