#ifndef __EYES_H
#define __EYES_H

#include <stdint.h>
#include "tm_oled.h"
#include "badge_inputs.h"

void Eyes_Init(oled_context_t * ctx_left, oled_context_t * ctx_right, badge_input_t wakeup_input);

void Eyes_StateClosed();
void Eyes_StateClosedLow();
void Eyes_StateClosedHalf();
void Eyes_StateBrow();
void Eyes_StateBrowLow();
void Eyes_StateOpened();
void Eyes_StateWinked();
void Eyes_StateDope();
void Eyes_StateLeft();
void Eyes_StateRight();
void Eyes_StateOutside();
void Eyes_StateInside();
void Eyes_StateAngryHalf();
void Eyes_StateAngry();
void Eyes_StateSadHalf();
void Eyes_StateSad();
void Eyes_Sleep();
void Eyes_Wake();
void Eyes_Blink();
void Eyes_Wonder();
void Eyes_Dummy();
void Eyes_Sad();
void Eyes_Angry();

#endif /* end of __EYES_H */
