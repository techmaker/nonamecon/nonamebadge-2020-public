#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "nvs_flash.h"
#include "settings.h"

esp_err_t settings_setU32(const char *name, uint32_t value) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
	esp_err_t err = nvs_set_u32(my_handle, name, value);
	nvs_commit(my_handle);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_getU32(const char *name, uint32_t *value) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
	esp_err_t err = nvs_get_u32(my_handle, name, value);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_setI32(const char *name, int32_t value) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
	esp_err_t err = nvs_set_i32(my_handle, name, value);
	nvs_commit(my_handle);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_getI32(const char *name, int32_t *value) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
	esp_err_t err = nvs_get_i32(my_handle, name, value);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_setString(const char *name, const char *value) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
	esp_err_t err = nvs_set_str(my_handle, name, value);
	nvs_commit(my_handle);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_getString(const char *name, char *value, size_t *maxLen) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
	esp_err_t err = nvs_get_str(my_handle, name, value, maxLen);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_setBlob(const char *name, const void *value, size_t length) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READWRITE, &my_handle);
	esp_err_t err = nvs_set_blob(my_handle, name, value, length);
	nvs_commit(my_handle);
	nvs_close(my_handle);

	return err;
}

esp_err_t settings_getBlob(const char *name, void *value, size_t *maxLen) {
	nvs_handle my_handle;
	nvs_open(SETSNAMESPACE, NVS_READONLY, &my_handle);
	esp_err_t err = nvs_get_blob(my_handle, name, value, maxLen);
	nvs_close(my_handle);

	return err;
}