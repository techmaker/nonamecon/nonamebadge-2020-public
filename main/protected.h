#pragma once

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "obfuscator.h"

bool protected_isEqual(const char *s, char *protected_s);
inline int protected_cmp(char *a, char *b, int len){
    if (a == NULL || b == NULL) return -1;
	int acc = 0;

	for (int i = 0; i < len; i++) {
		acc += a[i] ^ b[i];
	}

	return acc;
}
