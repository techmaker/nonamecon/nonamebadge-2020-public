#include "sensors.h"

#include <string.h>

#include "bme680.h"
#include "bsec_integration.h"
#include "esp_log.h"
#include "esp_spi_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "i2c_drv.h"
#include "settings.h"

static const char *TAG = "SENSORS";

// I2C driver handle
static i2c_drv_t *i2c_drv;

// BME write
static inline int8_t bus_write(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data_ptr, uint16_t data_len) {
	return i2c_drv_write_reg(i2c_drv, dev_addr, reg_addr, 1, reg_data_ptr, data_len);
}
// BME read
static inline int8_t bus_read(uint8_t dev_addr, uint8_t reg_addr, uint8_t *reg_data_ptr, uint16_t data_len) {
	return i2c_drv_read_reg(i2c_drv, dev_addr, reg_addr, 1, reg_data_ptr, data_len);
}

// BME_sleep
static void sensors_sleep(uint32_t t_ms) {
	vTaskDelay(t_ms / portTICK_PERIOD_MS);
}

static int64_t get_timestamp_us() {
	return esp_timer_get_time();
}

static sensors_data_t _state = {0};

void sensors_getValues(sensors_data_t *values) {
	memcpy(values, &_state, sizeof(sensors_data_t));
}

/*!
 * @brief           Handling of the ready outputs
 *
 * @param[in]       timestamp       time in nanoseconds
 * @param[in]       iaq             IAQ signal
 * @param[in]       iaq_accuracy    accuracy of IAQ signal
 * @param[in]       temperature     temperature signal
 * @param[in]       humidity        humidity signal
 * @param[in]       pressure        pressure signal
 * @param[in]       raw_temperature raw temperature signal
 * @param[in]       raw_humidity    raw humidity signal
 * @param[in]       gas             raw gas sensor signal
 * @param[in]       bsec_status     value returned by the bsec_do_steps() call
 *
 * @return          none
 */
static void output_ready(int64_t timestamp, float iaq, uint8_t iaq_accuracy,
                         float temperature, float humidity, float pressure,
                         float raw_temperature, float raw_humidity, float gas,
                         bsec_library_return_t bsec_status, float static_iaq,
                         float co2_equivalent, float breath_voc_equivalent) {
	_state.humidity = humidity;
	_state.iaq = iaq;
	_state.pressure = pressure;
	_state.temperature = temperature;
}

/*!
 * @brief           Load previous library state from non-volatile memory
 *
 * @param[in,out]   state_buffer    buffer to hold the loaded state string
 * @param[in]       n_buffer        size of the allocated state buffer
 *
 * @return          number of bytes copied to state_buffer
 */
static uint32_t state_load(uint8_t *state_buffer, uint32_t n_buffer) {
	size_t readout_size = n_buffer;
	return 0;

	esp_err_t err = settings_getBlob("bme680", state_buffer, &readout_size);
	if (err == ESP_OK) {
		ESP_LOGW(TAG, "Loaded bme state %d", readout_size);
		return readout_size;
	}
}

/*!
 * @brief           Save library state to non-volatile memory
 *
 * @param[in]       state_buffer    buffer holding the state to be stored
 * @param[in]       length          length of the state string to be stored
 *
 * @return          none
 */
static void state_save(const uint8_t *state_buffer, uint32_t length) {
	settings_setBlob("bme680", state_buffer, length);
	ESP_LOGW(TAG, "Saved bme state %d", length);
}

/*!
 * @brief           Load library config from non-volatile memory
 *
 * @param[in,out]   config_buffer    buffer to hold the loaded state string
 * @param[in]       n_buffer        size of the allocated state buffer
 *
 * @return          number of bytes copied to config_buffer
 */
static uint32_t config_load(uint8_t *config_buffer, uint32_t n_buffer) {
	// ...
	// Load a library config from non-volatile memory, if available.
	//
	// Return zero if loading was unsuccessful or no config was available,
	// otherwise return length of loaded config string.
	// ...
	return 0;
}

static int sensors_init() {
	if (!i2c_drv->ready) {
		ESP_LOGE(TAG, "i2c driver not ready!");
	}

	return_values_init ret;

	/* 
	 *	Call to the function which initializes the BSEC library
	 *	Switch on low-power mode and provide no temperature offset
	 */
	ret = bsec_iot_init(BSEC_SAMPLE_RATE_LP, 0.0f, bus_write, bus_read, sensors_sleep, state_load, config_load);
	if (ret.bme680_status) {
		/* Could not intialize BME680 */
		ESP_LOGE(TAG, "Could not intialize BME680 0x%x", ret.bme680_status);
		return (int)ret.bme680_status;
	} else if (ret.bsec_status) {
		/* Could not intialize BSEC library */
		ESP_LOGE(TAG, "Could not intialize BSEC library 0x%x", ret.bsec_status);
		return (int)ret.bsec_status;
	}

	return 0;
}

void taskSensors(void *params) {
	i2c_drv = (i2c_drv_t *)params;

	sensors_init();

	/* Call to endless loop function which reads and processes data based on sensor settings */
	/* State is saved every 10.000 samples, which means every 10.000 * 3 secs = 500 minutes  */
	bsec_iot_loop(sensors_sleep, get_timestamp_us, output_ready, state_save, 240);
	vTaskDelete(NULL);
}