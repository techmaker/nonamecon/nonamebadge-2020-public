#include "bsp.h"

#include <string.h>

#include "bme680.h"
#include "bsp_pins.h"
#include "esp_err.h"
#include "esp_log.h"
#include "pca9539.h"
#include "tm_oled.h"
#include "settings.h"

#define TOUCH_PAD_NO_CHANGE (-1)
#define TOUCH_THRESH_NO_USE (0)
#define TOUCH_FILTER_MODE_EN (1)
#define TOUCH_FILTER_PERIOD (10)
#define TOUCH_THRESH_PRESSED (500)

typedef enum {
	bsp_dev_none = 0x00,
	bsp_dev_touch = 0x01,
	bsp_dev_ws2812 = 0x02,
	bsp_dev_i2c1_bme680 = 0x04,
	bsp_dev_i2c1_joystick = 0x08,
	bsp_dev_i2c1_oled_left = 0x10,
	bsp_dev_i2c1_oled_right = 0x20,
	bsp_dev_i2c2_oled_left = 0x40,
	bsp_dev_i2c2_oled_right = 0x80,
	bsp_dev_regular = bsp_dev_touch | bsp_dev_ws2812,
	bsp_dev_i2c1_supporter_red = bsp_dev_i2c1_bme680 | bsp_dev_i2c1_joystick | bsp_dev_i2c1_oled_left | bsp_dev_i2c1_oled_right,
	bsp_dev_i2c1_supporter_black = bsp_dev_i2c1_bme680 | bsp_dev_i2c1_joystick,
	bsp_dev_i2c2_supporter_black = bsp_dev_i2c2_oled_left | bsp_dev_i2c2_oled_right,
	bsp_dev_i2c1_supporter_mask = bsp_dev_i2c1_supporter_red,
	bsp_dev_i2c2_supporter_mask = bsp_dev_i2c2_supporter_black,
} bsp_dev_t;

static const char *TAG = "BSP";

bsp_config_t bsp_config;
static bsp_dev_t bsp_devices;

static esp_err_t i2c_master_init(i2c_drv_t **drv, i2c_port_t port, gpio_num_t scl_pin, gpio_num_t sda_pin) {
	*drv = malloc(sizeof(i2c_drv_t));
	if (*drv == NULL) {
		ESP_ERROR_CHECK(ESP_ERR_NO_MEM);
	}
	memset(*drv, 0, sizeof(i2c_drv_t));
	(*drv)->i2c_port = port;
	(*drv)->i2c_clk_speed = 400000;
	(*drv)->i2c_scl_pin = scl_pin;
	(*drv)->i2c_sda_pin = sda_pin;
	(*drv)->i2c_timeout = 100 / portTICK_RATE_MS;
	(*drv)->mutex_timeout = portMAX_DELAY;
	return i2c_drv_init(*drv);
}

static esp_err_t i2c_master_deinit(i2c_drv_t *drv) {
	esp_err_t result = i2c_drv_deinit(bsp_config.i2c_drv_oled);
	free(bsp_config.i2c_drv_oled);
	return result;
}

static esp_err_t touch_init() {
	gpio_config_t io_conf;
	/* JTAG pins have to be disabled */
	io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
	io_conf.mode = GPIO_MODE_INPUT;
	io_conf.pin_bit_mask = 1ULL << bsp_config.pins.jtag[0] |
	                       1ULL << bsp_config.pins.jtag[1] |
	                       1ULL << bsp_config.pins.jtag[2] |
	                       1ULL << bsp_config.pins.jtag[3];
	io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
	io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&io_conf);
	ESP_ERROR_CHECK(touch_pad_init());
	ESP_ERROR_CHECK(touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V));
	ESP_ERROR_CHECK(touch_pad_config(BSP_BLACK_TOUCH_CHIPRIGHT, TOUCH_THRESH_NO_USE));
	ESP_ERROR_CHECK(touch_pad_config(BSP_RED_TOUCH_CHIPRIGHT, TOUCH_THRESH_NO_USE));
	ESP_ERROR_CHECK(touch_pad_filter_start(TOUCH_FILTER_PERIOD));
	return ESP_OK;
}

static bool touch_is_pressed(touch_pad_t pad) {
	uint16_t touch_filter_value;
	touch_pad_read_filtered(pad, &touch_filter_value);
	return touch_filter_value < TOUCH_THRESH_PRESSED;
}

static esp_err_t touch_deinit() {
	ESP_ERROR_CHECK(touch_pad_filter_delete());
	ESP_ERROR_CHECK(touch_pad_deinit());
	return ESP_OK;
}

static void bsp_pins_set(void) {
	if (bsp_config.type & bsp_type_black) {
		bsp_config.pins.boot = BSP_BLACK_PIN_BOOT;
		bsp_config.pins.led = BSP_BLACK_PIN_LED;
		bsp_config.pins.ws2812 = BSP_BLACK_PIN_WS2812;
		bsp_config.pins.i2c_scl = BSP_BLACK_PIN_IIC_SCL;
		bsp_config.pins.i2c_sda = BSP_BLACK_PIN_IIC_SDA;
		bsp_config.pins.io_rst = BSP_BLACK_PIN_IO_RST;
		bsp_config.pins.io_int = BSP_BLACK_PIN_IO_INT;
		bsp_config.pins.io_x = BSP_BLACK_PIN_IO_X;
		bsp_config.pins.io_y = BSP_BLACK_PIN_IO_Y;
		bsp_config.pins.oled_scl = BSP_BLACK_PIN_OLED_SCL;
		bsp_config.pins.oled_sda = BSP_BLACK_PIN_OLED_SDA;
		bsp_config.pins.can_tx = BSP_BLACK_PIN_CAN_TX;
		bsp_config.pins.can_rx = BSP_BLACK_PIN_CAN_RX;
		bsp_config.pins.jtag[0] = BSP_BLACK_PIN_JTAG_TCK;
		bsp_config.pins.jtag[1] = BSP_BLACK_PIN_JTAG_TDO;
		bsp_config.pins.jtag[2] = BSP_BLACK_PIN_JTAG_TDI;
		bsp_config.pins.jtag[3] = BSP_BLACK_PIN_JTAG_TMS;
		bsp_config.pins.vbat = BSP_BLACK_ADC_VBAT;
		bsp_config.pins.touch[0] = BSP_BLACK_TOUCH_CHIPLEFT;
		bsp_config.pins.touch[1] = BSP_BLACK_TOUCH_CHIPRIGHT;
		bsp_config.pins.touch[2] = BSP_BLACK_TOUCH_UP;
		bsp_config.pins.touch[3] = BSP_BLACK_TOUCH_DOWN;
		bsp_config.pins.touch[4] = BSP_BLACK_TOUCH_LEFT;
		bsp_config.pins.touch[5] = BSP_BLACK_TOUCH_RIGHT;
	} else if (bsp_config.type & bsp_type_red) {
		bsp_config.pins.boot = BSP_RED_PIN_BOOT;
		bsp_config.pins.led = BSP_RED_PIN_LED;
		bsp_config.pins.ws2812 = BSP_RED_PIN_WS2812;
		bsp_config.pins.i2c_scl = BSP_RED_PIN_IIC_SCL;
		bsp_config.pins.i2c_sda = BSP_RED_PIN_IIC_SDA;
		bsp_config.pins.io_rst = BSP_RED_PIN_IO_RST;
		bsp_config.pins.io_int = BSP_RED_PIN_IO_INT;
		bsp_config.pins.io_x = BSP_RED_PIN_IO_X;
		bsp_config.pins.io_y = BSP_RED_PIN_IO_Y;
		bsp_config.pins.oled_scl = BSP_RED_PIN_OLED_SCL;
		bsp_config.pins.oled_sda = BSP_RED_PIN_OLED_SDA;
		bsp_config.pins.can_tx = BSP_RED_PIN_CAN_TX;
		bsp_config.pins.can_rx = BSP_RED_PIN_CAN_RX;
		bsp_config.pins.jtag[0] = BSP_RED_PIN_JTAG_TCK;
		bsp_config.pins.jtag[1] = BSP_RED_PIN_JTAG_TDO;
		bsp_config.pins.jtag[2] = BSP_RED_PIN_JTAG_TDI;
		bsp_config.pins.jtag[3] = BSP_RED_PIN_JTAG_TMS;
		bsp_config.pins.vbat = BSP_RED_ADC_VBAT;
		bsp_config.pins.touch[0] = BSP_RED_TOUCH_CHIPLEFT;
		bsp_config.pins.touch[1] = BSP_RED_TOUCH_CHIPRIGHT;
		bsp_config.pins.touch[2] = BSP_RED_TOUCH_LEFTUP;
		bsp_config.pins.touch[3] = BSP_RED_TOUCH_LEFTDOWN;
		bsp_config.pins.touch[4] = BSP_RED_TOUCH_RIGHTUP;
		bsp_config.pins.touch[5] = BSP_RED_TOUCH_RIGHTDOWN;
	}
}

esp_err_t bsp_init(void) {
	uint8_t dev_i2c1_list[128];
	uint8_t dev_i2c1_cnt;
	uint8_t dev_i2c2_list[128];
	uint8_t dev_i2c2_cnt;

	esp_err_t err = settings_getI32("bsp_type", &bsp_config.type);
	if (ESP_OK != err) {
		touch_init();
		bsp_config.type = bsp_type_black;
		while (1) {
			if (touch_is_pressed(BSP_BLACK_TOUCH_CHIPRIGHT)) {
				bsp_config.type = bsp_type_black;
				break;
			}
			if (touch_is_pressed(BSP_RED_TOUCH_CHIPRIGHT)) {
				bsp_config.type = bsp_type_red;
				break;
			}
			printf("[BSP badge type detection] Press RIGHT CHIP on the badge to continue\n");
			vTaskDelay(20 / portTICK_RATE_MS);
		}
		touch_deinit();
		settings_setI32("bsp_type", bsp_config.type);
	}

	/* Check for I2C devices on Supporter Edition badges */
	ESP_ERROR_CHECK(i2c_master_init(&bsp_config.i2c_drv_main, I2C_NUM_0, BSP_BLACK_PIN_IIC_SCL, BSP_BLACK_PIN_IIC_SDA));
	ESP_ERROR_CHECK(i2c_drv_scanbus(bsp_config.i2c_drv_main, dev_i2c1_list, &dev_i2c1_cnt));
	printf("I2C%d: detected %d devices\r\n", I2C_NUM_0, dev_i2c1_cnt);
	for (uint8_t dev = 0; dev < dev_i2c1_cnt; dev++) {
		switch (dev_i2c1_list[dev]) {
		case PCA9539_I2C_ADDRESS1:
			bsp_devices |= bsp_dev_i2c1_joystick;
			printf("      [0x%02X] GPIO expander (Joysticks)\r\n", dev_i2c1_list[dev]);
			break;
		case BME680_I2C_ADDR_PRIMARY:
		case BME680_I2C_ADDR_SECONDARY:
			bsp_devices |= bsp_dev_i2c1_bme680;
			printf("      [0x%02X] BME680\r\n", dev_i2c1_list[dev]);
			break;
		case OLED_I2C_ADDR_1:
			bsp_devices |= bsp_dev_i2c1_oled_right;
			printf("      [0x%02X] OLED right\r\n", dev_i2c1_list[dev]);
			break;
		case OLED_I2C_ADDR_2:
			bsp_devices |= bsp_dev_i2c1_oled_left;
			printf("      [0x%02X] OLED left\r\n", dev_i2c1_list[dev]);
			break;
		default:
			printf("      [0x%02X] unknown device\r\n", dev_i2c1_list[dev]);
			break;
		}
	}
	ESP_ERROR_CHECK(i2c_master_init(&bsp_config.i2c_drv_oled, I2C_NUM_1, BSP_BLACK_PIN_OLED_SCL, BSP_BLACK_PIN_OLED_SDA));
	ESP_ERROR_CHECK(i2c_drv_scanbus(bsp_config.i2c_drv_oled, dev_i2c2_list, &dev_i2c2_cnt));
	printf("I2C%d: detected %d devices\r\n", I2C_NUM_1, dev_i2c2_cnt);
	for (uint8_t dev = 0; dev < dev_i2c2_cnt; dev++) {
		switch (dev_i2c2_list[dev]) {
		case OLED_I2C_ADDR_1:
			printf("      [0x%02X] OLED right\r\n", dev_i2c2_list[dev]);
			bsp_devices |= bsp_dev_i2c2_oled_right;
			break;
		case OLED_I2C_ADDR_2:
			printf("      [0x%02X] OLED left\r\n", dev_i2c2_list[dev]);
			bsp_devices |= bsp_dev_i2c2_oled_left;
			break;
		default:
			printf("      [0x%02X] unknown device\r\n", dev_i2c2_list[dev]);
			break;
		}
	}

	if ((bsp_devices & bsp_dev_i2c1_supporter_mask) == bsp_dev_i2c1_supporter_red) {
		/* Red Badge Supporter Edition, a rare one ;) */
		/* Use I2C1 for OLEDs as well */
		i2c_master_deinit(bsp_config.i2c_drv_oled);
		bsp_config.i2c_drv_oled = bsp_config.i2c_drv_main;
		bsp_config.type = bsp_type_red | bsp_type_supporter;
		ESP_LOGI(TAG, "Red Badge Supporter Edition");
		printf("Red Badge Supporter Edition\r\n");
	} else if ((bsp_devices & bsp_dev_i2c1_supporter_mask) == bsp_dev_i2c1_supporter_black) {
		/* Black Badge Supporter Edition */
		/* Check I2C2 for OLEDs */
		if ((bsp_devices & bsp_dev_i2c2_supporter_mask) == bsp_dev_i2c2_supporter_black ||
		    (bsp_devices & bsp_dev_i2c2_supporter_mask) == bsp_dev_i2c2_oled_right) {
			bsp_config.type = bsp_type_black | bsp_type_supporter;
			ESP_LOGI(TAG, "Black Badge Supporter Edition");
			printf("Black Badge Supporter Edition\r\n");
		} else {
			ESP_LOGI(TAG, "Custom Badge?");
			printf("Custom Badge?\r\n");
		}
	} else {
		/* Regular Edition */
		if (bsp_is_red()) {
			ESP_LOGI(TAG, "Red Badge");
			printf("Red Badge\r\n");
		} else {
			ESP_LOGI(TAG, "Black Badge");
			printf("Black Badge\r\n");
		}
	}

	bsp_pins_set();
	return ESP_OK;
}

bool bsp_is_supporter(void) {
	return (bsp_config.type & bsp_type_supporter) == bsp_type_supporter;
}

bool bsp_has_sensors(void) {
	return (bsp_devices & bsp_dev_i2c1_bme680) == bsp_dev_i2c1_bme680;
}

bool bsp_has_joystick(void) {
	return (bsp_devices & bsp_dev_i2c1_joystick) == bsp_dev_i2c1_joystick;
}

bool bsp_has_display(void) {
	return (bsp_devices & bsp_dev_i2c2_oled_left) == bsp_dev_i2c2_oled_left ||
	       (bsp_devices & bsp_dev_i2c2_oled_right) == bsp_dev_i2c2_oled_right ||
		   (bsp_devices & bsp_dev_i2c1_oled_left) == bsp_dev_i2c1_oled_left ||
	       (bsp_devices & bsp_dev_i2c1_oled_right) == bsp_dev_i2c1_oled_right;
}

bool bsp_is_red(void) {
	return (bsp_config.type & bsp_type_red) == bsp_type_red;
}
