#pragma once

#include "esp_err.h"
#include <driver/gpio.h>
#include <driver/adc.h>
#include <driver/touch_pad.h>
#include "i2c_drv.h"

typedef enum {
	bsp_type_red = 	0x01,
	bsp_type_black = 0x02,
	bsp_type_supporter = 0x04,
} bsp_type_t;

typedef struct {
	gpio_num_t boot;
	gpio_num_t led;
	gpio_num_t ws2812;
	gpio_num_t i2c_scl;
	gpio_num_t i2c_sda;
	gpio_num_t io_rst;
	gpio_num_t io_int;
	gpio_num_t io_x;
	gpio_num_t io_y;
	gpio_num_t oled_scl;
	gpio_num_t oled_sda;
	gpio_num_t can_tx;
	gpio_num_t can_rx;
	gpio_num_t jtag[4];
	adc_channel_t vbat;
	touch_pad_t touch[6];
} bsp_pins_t;

typedef struct {
	bsp_type_t type;
	bsp_pins_t pins;
	i2c_drv_t * i2c_drv_main;
	i2c_drv_t * i2c_drv_oled;
} bsp_config_t;

extern bsp_config_t bsp_config;

esp_err_t bsp_init(void);
bool bsp_is_supporter(void);
bool bsp_is_red(void);
bool bsp_has_sensors(void);
bool bsp_has_joystick(void);
bool bsp_has_display(void);