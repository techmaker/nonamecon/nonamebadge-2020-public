#ifndef WS2812_CONTROL_H
#define WS2812_CONTROL_H
#include <stdint.h>

#include "bsp.h"

// Configure these based on your project needs ********
#define LED_NUM 18
#define LED_RMT_TX_CHANNEL RMT_CHANNEL_0
// ****************************************************

#define BITS_PER_LED_CMD 24
#define LED_BUFFER_ITEMS ((LED_NUM * BITS_PER_LED_CMD) + 1)


#define TICK_DIV (25)

// These values are determined by measuring pulse timing with logic analyzer and
// adjusting to match datasheet.
#define WS2812_T0H_NS (400 / TICK_DIV)
#define WS2812_T0L_NS (850 / TICK_DIV)
#define WS2812_T1H_NS (800 / TICK_DIV)
#define WS2812_T1L_NS (450 / TICK_DIV)
#define WS2812_RESET_US (100000 / TICK_DIV)

// This structure is used for indicating what the colors of each LED should be
// set to. There is a 32bit value for each LED. Only the lower 3 bytes are used
// and they hold the Red (byte 2), Green (byte 1), and Blue (byte 0) values to
// be set.
typedef struct {
	uint32_t leds[LED_NUM];
	bool blink_effect;
	bool breath_effect;
	bool only_cross;
} led_state_t;

// Setup the hardware peripheral. Only call this once.
void ws2812_control_init(void);

// Update the LEDs to the new state. Call as needed.
// This function will block the current task until the RMT peripheral is
// finished sending the entire sequence.
void ws2812_write_leds(led_state_t *new_state);

#endif