#pragma once

#include <stdint.h>

#define GREEN 0x00FF00
#define RED 0xFF0000
#define BLUE 0x0000FF

void led_setBrightness(int perc);
// if customBightness > 0 it will be used insted of user defined brightness
void led_setColor(int ledIndex, uint32_t color, int customBrightness);
void led_updateAll();