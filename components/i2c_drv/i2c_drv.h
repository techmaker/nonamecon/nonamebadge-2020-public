#ifndef __I2C_DRV_H
#define __I2C_DRV_H

#include <stddef.h>
#include <stdint.h>
#include "esp_err.h"
#include "driver/gpio.h"
#include "driver/i2c.h"
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

typedef struct {
	bool ready;
	i2c_port_t i2c_port;
	uint32_t i2c_clk_speed;
	gpio_num_t i2c_scl_pin;
	gpio_num_t i2c_sda_pin;
	TickType_t i2c_timeout;
	SemaphoreHandle_t mutex;
	TickType_t mutex_timeout;
} i2c_drv_t;

esp_err_t i2c_drv_init(i2c_drv_t * drv);
esp_err_t i2c_drv_deinit(i2c_drv_t * drv);
esp_err_t i2c_drv_scanbus(i2c_drv_t * drv, uint8_t * dev_list, uint8_t * dev_cnt);
esp_err_t i2c_drv_write_byte(i2c_drv_t * drv, uint8_t dev_addr, uint8_t data);
esp_err_t i2c_drv_write_bytes(i2c_drv_t * drv, uint8_t dev_addr, uint8_t * data, size_t data_len);
esp_err_t i2c_drv_write_reg(i2c_drv_t * drv, uint8_t dev_addr, uint16_t reg_addr, uint8_t reg_addr_len, uint8_t * data, size_t data_len);
esp_err_t i2c_drv_read_byte(i2c_drv_t * drv, uint8_t dev_addr, uint8_t * data);
esp_err_t i2c_drv_read_bytes(i2c_drv_t * drv, uint8_t dev_addr, uint8_t * data, size_t data_len);
esp_err_t i2c_drv_read_reg(i2c_drv_t * drv, uint8_t dev_addr, uint16_t reg_addr, uint8_t reg_addr_len, uint8_t * data, size_t data_len);
#endif /* end of __I2C_DRV_H */