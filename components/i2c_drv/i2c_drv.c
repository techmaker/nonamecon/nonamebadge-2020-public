#include "i2c_drv.h"
#include "esp_err.h"
#include "esp_log.h"

static const char * TAG = "i2c_drv";

esp_err_t i2c_drv_init(i2c_drv_t * drv) {
	if (drv == NULL || drv->ready) {
		return ESP_FAIL;
	}	
	i2c_config_t i2c_config = {
		.mode = I2C_MODE_MASTER,
		.sda_io_num = drv->i2c_sda_pin,
		.scl_io_num = drv->i2c_scl_pin,
		.sda_pullup_en = GPIO_PULLUP_ENABLE,
		.scl_pullup_en = GPIO_PULLUP_ENABLE,
		.master.clk_speed = drv->i2c_clk_speed
	};					   
	drv->mutex = xSemaphoreCreateMutex();	
	if (drv->mutex == NULL) {
		drv->ready = false;
		return ESP_FAIL;
	}					   
	ESP_LOGI(TAG, "Init I2C%d master @%dHz on pins %d[SCL] %d[SDA]]", 
					drv->i2c_port, drv->i2c_clk_speed, drv->i2c_scl_pin, drv->i2c_sda_pin);
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		ESP_ERROR_CHECK(i2c_param_config(drv->i2c_port, &i2c_config));
		ESP_ERROR_CHECK(i2c_driver_install(drv->i2c_port, i2c_config.mode, 0, 0, 0));
		xSemaphoreGive(drv->mutex);
		drv->ready = true;
		return ESP_OK;
	} else {
		drv->ready = false;
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_deinit(i2c_drv_t * drv) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}					   
	ESP_LOGI(TAG, "Deinit I2C%d master on pins %d[SCL] %d[SDA]]", 
					drv->i2c_port, drv->i2c_scl_pin, drv->i2c_sda_pin);
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		ESP_ERROR_CHECK(i2c_driver_delete(drv->i2c_port));
		vSemaphoreDelete(drv->mutex);
		drv->mutex = NULL;
		drv->ready = false;
		return ESP_OK;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_scanbus(i2c_drv_t * drv, uint8_t * dev_list, uint8_t * dev_cnt) {
	if (drv == NULL || !drv->ready || dev_list == NULL || dev_cnt == NULL) {
		return ESP_FAIL;
	}	
	uint8_t cnt = 0;
	esp_err_t ret = ESP_ERR_NOT_FOUND;
	ESP_LOGI(TAG, "started scan on I2C%d bus", drv->i2c_port);
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		for (uint8_t dev_addr = 0x08; dev_addr < 0x78; dev_addr++) {
			i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
			ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
			ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_WRITE, true));
			ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
			esp_err_t status = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
			i2c_cmd_link_delete(cmd_handle);
			if (status == ESP_OK) {
				ESP_LOGI(TAG, "detected: 0x%02X", dev_addr);
				dev_list[cnt++] = dev_addr;
				ret = ESP_OK;
			} 
		}
		*dev_cnt = cnt;
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_write_byte(i2c_drv_t * drv, uint8_t dev_addr, uint8_t data) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}	
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_WRITE, true));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, data, true));
		ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
		esp_err_t ret = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
		i2c_cmd_link_delete(cmd_handle);
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_write_bytes(i2c_drv_t * drv, uint8_t dev_addr, uint8_t * data, size_t data_len) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}	
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_WRITE, true));
		ESP_ERROR_CHECK(i2c_master_write(cmd_handle, data, data_len, true));
		ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
		esp_err_t ret = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
		i2c_cmd_link_delete(cmd_handle);
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_write_reg(i2c_drv_t * drv, uint8_t dev_addr, uint16_t reg_addr, uint8_t reg_addr_len, uint8_t * data, size_t data_len) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}	
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_WRITE, true));
		ESP_ERROR_CHECK(i2c_master_write(cmd_handle, (uint8_t *) &reg_addr, reg_addr_len, true));
		ESP_ERROR_CHECK(i2c_master_write(cmd_handle, data, data_len, true));
		ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
		esp_err_t ret = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
		i2c_cmd_link_delete(cmd_handle);
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_read_byte(i2c_drv_t * drv, uint8_t dev_addr, uint8_t * data) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}	
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_READ, true));
		ESP_ERROR_CHECK(i2c_master_read_byte(cmd_handle, data, I2C_MASTER_LAST_NACK));
		ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
		esp_err_t ret = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
		i2c_cmd_link_delete(cmd_handle);
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {	
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_read_bytes(i2c_drv_t * drv, uint8_t dev_addr, uint8_t * data, size_t data_len) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}	
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_READ, true));
		ESP_ERROR_CHECK(i2c_master_read(cmd_handle, data, data_len, I2C_MASTER_LAST_NACK));
		ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
		esp_err_t ret = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
		i2c_cmd_link_delete(cmd_handle);
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}

esp_err_t i2c_drv_read_reg(i2c_drv_t * drv, uint8_t dev_addr, uint16_t reg_addr, uint8_t reg_addr_len, uint8_t * data, size_t data_len) {
	if (drv == NULL || !drv->ready) {
		return ESP_FAIL;
	}	
	if (xSemaphoreTake(drv->mutex, (TickType_t) drv->mutex_timeout) == pdTRUE) {
		i2c_cmd_handle_t cmd_handle = i2c_cmd_link_create();
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_WRITE, true));
		ESP_ERROR_CHECK(i2c_master_write(cmd_handle, (uint8_t *) &reg_addr, reg_addr_len, true));
		ESP_ERROR_CHECK(i2c_master_start(cmd_handle));
		ESP_ERROR_CHECK(i2c_master_write_byte(cmd_handle, (dev_addr << 1) | I2C_MASTER_READ, true));
		ESP_ERROR_CHECK(i2c_master_read(cmd_handle, data, data_len, I2C_MASTER_LAST_NACK));
		ESP_ERROR_CHECK(i2c_master_stop(cmd_handle));
		esp_err_t ret = i2c_master_cmd_begin(drv->i2c_port, cmd_handle, drv->i2c_timeout);
		i2c_cmd_link_delete(cmd_handle);
		xSemaphoreGive(drv->mutex);
		return ret;
	} else {
		return ESP_ERR_TIMEOUT;
	}
}