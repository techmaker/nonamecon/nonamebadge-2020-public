/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	PCA9539 I/O expander using I2C bus
 *	23 Mar 2020 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.0 initial version
 */

#include "pca9539.h"

static inline esp_err_t pca9539_writereg(pca9539_context_t *ctx, uint8_t reg, uint8_t *data, size_t len) {
	return i2c_drv_write_reg(ctx->i2c_drv, ctx->i2c_addr, reg, 1, data, len);
}

static inline esp_err_t pca9539_readreg(pca9539_context_t *ctx, uint8_t reg, uint8_t *data, size_t len) {
	return i2c_drv_read_reg(ctx->i2c_drv, ctx->i2c_addr, reg, 1, data, len);
}

esp_err_t pca9539_init(pca9539_context_t *ctx) {
	if (ctx->i2c_drv == NULL) {
		return ESP_ERR_INVALID_ARG;
	}
	gpio_config_t io_conf;
	/* Reset pin */
	if (ctx->rst_pin != GPIO_NUM_NC) {
		io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
		io_conf.mode = GPIO_MODE_OUTPUT;
		io_conf.pin_bit_mask = 1ULL << ctx->rst_pin;
		io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
		io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
		esp_err_t ret = gpio_config(&io_conf);
		if (ret != ESP_OK) {
			return ret;
		}
	}
	/* INT pin */
	if (ctx->int_pin != GPIO_NUM_NC) {
		io_conf.intr_type = GPIO_INTR_NEGEDGE;
		io_conf.mode = GPIO_MODE_INPUT;
		io_conf.pin_bit_mask = 1ULL << ctx->int_pin;
		io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
		io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
		esp_err_t ret = gpio_config(&io_conf);
		if (ret != ESP_OK) {
			return ret;
		}
	}
	pca9539_reset(ctx);
	return ESP_OK;
}
esp_err_t pca9539_reset(pca9539_context_t *ctx) {
	ESP_ERROR_CHECK(gpio_set_level(ctx->rst_pin, 0));
	vTaskDelay(1 / portTICK_RATE_MS);
	ESP_ERROR_CHECK(gpio_set_level(ctx->rst_pin, 1));
	vTaskDelay(1 / portTICK_RATE_MS);
	return ESP_OK;
}
esp_err_t pca9539_config_port(pca9539_context_t *ctx, pca9539_port_t port, pca9539_pin_t pin, pca9539_mode_t mode, pca9539_polarity_t polarity) {
	uint8_t reg_value = 0;
	/* Set mode */
	pca9539_readreg(ctx, PCA9539_CMD_CONFIG | port, &reg_value, 1);
	if (mode == PCA9539_INPUT) {
		reg_value |= pin;
	}
	else {
		reg_value &= ~pin;
	}
	pca9539_writereg(ctx, PCA9539_CMD_CONFIG | port, &reg_value, 1);
	/* Set inversion */
	pca9539_readreg(ctx, PCA9539_CMD_POLARITY | port, &reg_value, 1);
	if (polarity == PCA9539_INVERTED) {
		reg_value |= pin;
	}
	else {
		reg_value &= ~pin;
	}
	pca9539_writereg(ctx, PCA9539_CMD_POLARITY | port, &reg_value, 1);
	return ESP_OK;
}
esp_err_t pca9539_int_enable(pca9539_context_t *ctx) {
	pca9539_read_all(ctx);
	gpio_install_isr_service(ESP_INTR_FLAG_EDGE);
	gpio_isr_handler_add(ctx->int_pin, ctx->isr_handler, NULL);
	return ESP_OK;
}
esp_err_t pca9539_int_disable(pca9539_context_t *ctx) {
	gpio_isr_handler_remove(ctx->int_pin);
	return ESP_OK;
}
void pca9539_write_all(pca9539_context_t *ctx, uint16_t state) {
	pca9539_writereg(ctx, PCA9539_CMD_OUTPUT, (uint8_t *) &state, 2);
}
void pca9539_write_port(pca9539_context_t *ctx, pca9539_port_t port, uint8_t state) {
	pca9539_writereg(ctx, PCA9539_CMD_OUTPUT | port, &state, 1);
}
void pca9539_write_pin(pca9539_context_t *ctx, pca9539_port_t port, pca9539_pin_t pin, uint8_t state) {
	uint8_t reg_value = 0;
	pca9539_readreg(ctx, PCA9539_CMD_OUTPUT | port, &reg_value, 1);
	if (state) {
		reg_value |= pin;
	}
	else {
		reg_value &= ~pin;
	}
	pca9539_writereg(ctx, PCA9539_CMD_OUTPUT | port, &reg_value, 1);
}
uint16_t pca9539_read_all(pca9539_context_t *ctx) {
	uint16_t state = 0;
	pca9539_readreg(ctx, PCA9539_CMD_INPUT, (uint8_t *) &state, 2);
	return state;
}
uint8_t pca9539_read_port(pca9539_context_t *ctx, pca9539_port_t port) {
	uint8_t state = 0;
	pca9539_readreg(ctx, PCA9539_CMD_INPUT | port, &state, 1);
	return state;
}
uint8_t pca9539_read_pin(pca9539_context_t *ctx, pca9539_port_t port, pca9539_pin_t pin) {
	uint8_t reg_value = pca9539_read_port(ctx, port);
	return (reg_value & pin) == pin;
}