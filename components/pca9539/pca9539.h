/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	PCA9539 I/O expander using I2C bus
 *	23 Mar 2020 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.0 initial version
 */

#ifndef __PCA9539_H
#define __PCA9539_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "esp_err.h"
#include "i2c_drv.h"
#include "driver/gpio.h"

#define PCA9539_CMD_INPUT		0x00
#define PCA9539_CMD_OUTPUT		0x02
#define PCA9539_CMD_POLARITY	0x04
#define PCA9539_CMD_CONFIG		0x06

typedef enum {
	PCA9539_PORT_0 = 0x00,
	PCA9539_PORT_1 = 0x01,
	PCA9539_PORT_MAX,
} pca9539_port_t;

typedef enum {
	PCA9539_PIN_0 = (1 << 0),
	PCA9539_PIN_1 = (1 << 1),
	PCA9539_PIN_2 = (1 << 2),
	PCA9539_PIN_3 = (1 << 3),
	PCA9539_PIN_4 = (1 << 4),
	PCA9539_PIN_5 = (1 << 5),
	PCA9539_PIN_6 = (1 << 6),
	PCA9539_PIN_7 = (1 << 7),
	PCA9539_PIN_ALL = 	PCA9539_PIN_0 | 
						PCA9539_PIN_1 | 
						PCA9539_PIN_2 | 
						PCA9539_PIN_3 | 
						PCA9539_PIN_4 | 
						PCA9539_PIN_5 | 
						PCA9539_PIN_6 | 
						PCA9539_PIN_7,
} pca9539_pin_t;

typedef enum {
	PCA9539_OUTPUT = 0x0,
	PCA9539_INPUT = 0x1,
} pca9539_mode_t;

typedef enum {
	PCA9539_NORMAL = 0x0,
	PCA9539_INVERTED = 0x1,
} pca9539_polarity_t;

typedef enum {
	PCA9539_I2C_ADDRESS1 = 0x74,
	PCA9539_I2C_ADDRESS2 = 0x75,
	PCA9539_I2C_ADDRESS3 = 0x76,
	PCA9539_I2C_ADDRESS4 = 0x77,
} pca9539_i2c_addr_t;

typedef struct {
	i2c_drv_t *i2c_drv;
	pca9539_i2c_addr_t i2c_addr;
	gpio_num_t rst_pin;
	gpio_num_t int_pin;
	gpio_isr_t isr_handler;
} pca9539_context_t;

esp_err_t pca9539_init(pca9539_context_t *ctx);
esp_err_t pca9539_reset(pca9539_context_t *ctx);
esp_err_t pca9539_config_port(pca9539_context_t *ctx, pca9539_port_t port, pca9539_pin_t pin, pca9539_mode_t mode, pca9539_polarity_t polarity);
esp_err_t pca9539_int_enable(pca9539_context_t *ctx);
esp_err_t
 pca9539_int_disable(pca9539_context_t *ctx);
void pca9539_write_all(pca9539_context_t *ctx, uint16_t state);
void pca9539_write_port(pca9539_context_t *ctx, pca9539_port_t port, uint8_t state);
void pca9539_write_pin(pca9539_context_t *ctx, pca9539_port_t port, pca9539_pin_t pin, uint8_t state);
uint16_t pca9539_read_all(pca9539_context_t *ctx);
uint8_t pca9539_read_port(pca9539_context_t *ctx, pca9539_port_t port);
uint8_t pca9539_read_pin(pca9539_context_t *ctx, pca9539_port_t port, pca9539_pin_t pin);

#endif /* end of __PCA9539_H */