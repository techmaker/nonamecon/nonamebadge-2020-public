#include "driver/adc.h"
#include "esp_adc_cal.h"

#define DEFAULT_VREF 1100
#define NO_OF_SAMPLES 100

static esp_adc_cal_characteristics_t *adc_chars = NULL;

int ibat_get_mV(adc1_channel_t adc_ch) {
	int raw = 0;
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(adc_ch, ADC_ATTEN_DB_11);

	if (adc_chars == NULL) {
		adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
		esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, DEFAULT_VREF, adc_chars);
	}

	for (int i = 0; i < NO_OF_SAMPLES; i++) {
		raw += adc1_get_raw(adc_ch);
	}
	raw /= NO_OF_SAMPLES;

	uint32_t mVoltage = esp_adc_cal_raw_to_voltage(raw, adc_chars);
	mVoltage /= 5;
	mVoltage *= 10;

	return mVoltage;
}