/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	STM32 LCD OLED Library for 0.96" / 1.3" displays using I2C/SPI
 *	based on Adafruit GFX & Adafruit TFT LCD libraries
 *	16 Aug 2018 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.2 added SPI interface & return status codes
 *		- v1.1 added Courier New font family with Cyrillic (CP1251), created using TheDotFactory font generator
 *		- v1.0 added support for SSD1306 and SH1106 chips
 */

#include "tm_oled.h"
#include "esp_log.h"

static font_t *fonts[] = {
#ifdef USE_FONT8
    &Font8,
#endif
#ifdef USE_FONT12
    &Font12,
#endif
#ifdef USE_FONT16
    &Font16,
#endif
#ifdef USE_FONT20
    &Font20,
#endif
#ifdef USE_FONT24
    &Font24,
#endif
};
const static uint8_t fontsNum = sizeof(fonts) / sizeof(fonts[0]);
#if defined (USE_LOOKUP)
static bool lookup_ready = false;
static size_t lookup_x[OLEDWIDTH] = { 0 };
static size_t lookup_y[3][OLEDHEIGHT] = { 0 };
#endif

#if defined(SSD1306)
static const uint8_t SSD1306_regValues[] = {
    SSD1306_SETDISPLAY_OFF, 0,                                               // Display off
#if defined(USE_HORZMODE)
    SSD1306_SETMEMORYMODE, 1, SSD1306_MEMORYMODE_HORZ,                       // Set Memory Addressing Mode to Horizontal
    SSD1306_SETCOLUMNADDR, 2, 0x00, OLEDWIDTH - 1,                           // Set column address window from 0 to 127
    SSD1306_SETPAGEADDR, 2, 0x00, 0x07,                                      // Set page address window from 0 to 7
#elif defined(USE_VERTMODE)
    SSD1306_SETMEMORYMODE, 1, SSD1306_MEMORYMODE_VERT,                       // Set Memory Addressing Mode to Vertical
    SSD1306_SETCOLUMNADDR, 2, 0x00, OLEDWIDTH - 1,                           // Set column address window from 0 to 127
    SSD1306_SETPAGEADDR, 2, 0x00, 0x07,                                      // Set page address window from 0 to 7
#elif defined(USE_PAGEMODE)
    SSD1306_SETCOLUMNSTARTLOW | 0x00, 0,                                      // Set column start address to 0 (lower nibble)
    SSD1306_SETCOLUMNSTARTHIGH | 0x00, 0,                                     // Set column start address to 0 (higher nibble)
    SSD1306_SETPAGESTART | 0x00, 0,                                           // Set page start address to 0 (use | 0x07 for 7th page)
#endif
    SSD1306_SETDISPLAYSTARTLINE | 0x00, 0,                                    // Set page start line to 0 (use | 0xFF for 63rd line)
    SSD1306_SETSEGREMAP_127, 0,                                               // Set segment re-map from 0 to 127
    SSD1306_SETDISPLAY_NORMAL, 0,                                             // Set normal display (not inverted)
    SSD1306_SETMUXRATIO, 1, OLEDHEIGHT - 1,                                   // Set multiplex ratio (default 63)
    SSD1306_SETCOMSCAN_DEC, 0,                                                // Set COM Output Scan Direction to Reverse
    SSD1306_SETDISPLAYOFFSET, 1, 0x00,                                        // Set display offset to 0
#if OLEDHEIGHT <= 32
    SSD1306_SETCOMPINS, 1, SSD1306_COMPINS_SEQUENTIAL,                        // Set COM pins hardware configuration: normal configuration / no left-right remap
#else
    SSD1306_SETCOMPINS, 1, SSD1306_COMPINS_ALTERNATIVE,                       // Set COM pins hardware configuration: alternative configuration / no left-right remap
#endif
    SSD1306_SETDISPLAYCLOCK, 1, SSD1306_OSCFREQ_534KHZ | SSD1306_CLOCKDIV_1,  // Set oscillator frequency to max, display clock divide ratio to 1 (1 DCLK = CLK)
    SSD1306_SETPRECHARGEPERIOD, 1, 0x33,                                      // Set pre-charge period: phase 1 period to 3DCLK, phase 2 period to 3DCLK
    SSD1306_SETCONTRAST, 1, 0x3F,                                             // Set contrast control register to 64
    SSD1306_SCROLL_DEACTIVATE, 0,                                             // Disable scrolling
    SSD1306_SETFADEOUTANDBLINK, 1, SSD1306_FADE_OFF,                          // Disable fading
    SSD1306_SETZOOMIN, 1, SSD1306_ZOOMIN_OFF,                                 // Disable zoom-in
    SSD1306_SETVCOMLEVEL, 1, SSD1306_VCOM_083VCC,                             // Set Vcomh deselect at 0.83*Vcc
    SSD1306_DISPLAY_RAM, 0,                                                   // Set output to follow RAM content
    SSD1306_SETCHARGEPUMP, 1, SSD1306_CHARGEPUMP_ON,                          // Enable charge pump
    SSD1306_SETDISPLAY_ON, 0                                                  // Display on
};
#elif defined(SH1106)
static const uint8_t SH1106_regValues[] = {
    SH1106_SETDISPLAY_OFF, 0,                                                 // Display off
    // SH1106 ram has 132px, OLED matrix is 128px, they are center aligned - so image has to be shifted 2px!!
    SH1106_SETCOLUMNSTARTLOW | 0x02, 0,                                       // Set low column address to 2
    SH1106_SETCOLUMNSTARTHIGH | 0x00, 0,                                      // Set high column address to 0
    SH1106_SETPAGESTART | 0x00, 0,                                            // Set Page Start Address to 0 (use | 0x07 for 7th Page)
    SH1106_SETDISPLAYSTARTLINE | 0x00, 0,                                     // Set page start line to 0 (use | 0xFF for 63rd line)
    SH1106_SETSEGREMAP_127, 0,                                                // Set segment re-map from 0 to 127
    SH1106_SETDISPLAY_NORMAL, 0,                                              // Set normal display (not inverted)
    SH1106_SETMUXRATIO, 1, OLEDHEIGHT - 1,                                    // Set multiplex ratio (default 63)
    SH1106_SETCOMSCAN_DEC, 0,                                                 // Set COM Output Scan Direction to Reverse
    SH1106_SETDISPLAYOFFSET, 1, 0x00,                                         // Set display offset to 0
#if OLEDHEIGHT <= 32
    SH1106_SETCOMPINS, 1, SH1106_COMPINS_SEQUENTIAL,                          // Set COM pins hardware configuration: normal configuration / no left-right remap
#else
    SH1106_SETCOMPINS, 1, SH1106_COMPINS_ALTERNATIVE,                         // Set COM pins hardware configuration: alternative configuration / no left-right remap
#endif
    SH1106_SETDISPLAYCLOCK, 1, SH1106_OSCFREQ_534KHZ | SH1106_CLOCKDIV_1,     // Set display clock divide ratio to 8 (8 DCLK = CLK), oscillator frequency to 260Hz
    SH1106_SETPRECHARGEPERIOD, 1, 0x33,                                       // Set pre-charge period: phase 1 period to 3DCLK, phase 2 period to 3DCLK
    SH1106_SETCONTRAST, 1, 0x3F,                                              // Set contrast control register to 64
    SH1106_DISPLAY_RAM, 0,                                                    // Set output to follow RAM content
    SH1106_SETVCOMLEVEL, 1, SH1106_SETVCOM_100V,                              // Set Vcomh level to 0.83xVCC
    SH1106_SETPUMPVOLTAGE, 1, SH1106_PUMPVOLTAGE_9V0,                         // Set charge pump voltage to 9.0V
    SH1106_SETDCDC, 1, SH1106_DCDC_ON,                                        // Enable charge pump
    SH1106_SETDISPLAY_ON, 0                                                   // Display on
};
#endif

/* ==================================================
 * ============= I2C Specific functions =============
 * ================================================== */

static inline esp_err_t OLED_WriteCmd(oled_context_t *ctx, uint8_t cmd) {
	if (ctx == NULL) {
		return ESP_ERR_INVALID_ARG;
	}
	return i2c_drv_write_reg(ctx->i2c_drv, ctx->i2c_addr, I2C_CONTROL_CMD, 1, &cmd, 1);
}

static inline esp_err_t OLED_WriteData(oled_context_t *ctx, uint8_t *data, uint32_t len) {
	if (ctx == NULL) {
		return ESP_ERR_INVALID_ARG;
	} 
	return i2c_drv_write_reg(ctx->i2c_drv, ctx->i2c_addr, I2C_CONTROL_DATA, 1, data, len);
}

esp_err_t OLED_Init(oled_context_t *ctx) {
	esp_err_t status = ESP_FAIL;
	if (ctx == NULL || ctx->i2c_drv == NULL) {
		return ESP_ERR_INVALID_ARG;
	}
	ctx->m_width = OLEDWIDTH;
	ctx->m_height = OLEDHEIGHT;
	ctx->m_cursor_y = ctx->m_cursor_x = 0;
	ctx->m_font = 0;
	ctx->m_textcolor = White;
	ctx->m_inverted = 0;
	ctx->m_wrap = 1;
	if (ctx->display_buf == NULL) {
		ctx->display_buf = malloc(ctx->m_width * ctx->m_height / 8);
	}	
#if defined (USE_LOOKUP)
	if (lookup_ready == false) {
#if defined(USE_HORZMODE) || defined(USE_PAGEMODE)
		for (size_t i = 0; i < OLEDWIDTH; i++) {
			lookup_x[i] = i;			
		}
		for (size_t i = 0; i < OLEDHEIGHT; i++) {
			lookup_y[0][i] = (i >> 3) * OLEDWIDTH;
			lookup_y[1][i] = 1 << (i & 0x7);
			lookup_y[2][i] = ~(1 << (i & 0x7));
		}
#elif defined(USE_PAGEMODE)
		for (size_t i = 0; i < OLEDWIDTH; i++) {
			lookup_x[i] = (x * OLEDHEIGHT) >> 3;
		}
		for (size_t i = 0; i < OLEDHEIGHT; i++) {
			lookup_y[0][i] = i >> 3;
			lookup_y[1][i] = 1 << (i & 0x7);
			lookup_y[2][i] = ~(1 << (i & 0x7));
		}
#endif	
		lookup_ready = true;
	}
#endif
	/* Set register values */
#if defined(SSD1306)
	uint8_t i = 0;
	while (i < sizeof(SSD1306_regValues) / sizeof(SSD1306_regValues[0])) {
		uint8_t r = SSD1306_regValues[i++];
		uint8_t len = SSD1306_regValues[i++];
		status += OLED_WriteCmd(ctx, r);
		for (uint8_t d = 0; d < len; d++) {
			uint8_t x = SSD1306_regValues[i++];
			status += OLED_WriteCmd(ctx, x);
		}
	}
#elif defined(SH1106)
	uint8_t i = 0;
	while (i < sizeof(SH1106_regValues) / sizeof(SH1106_regValues[0])) {
		uint8_t r = SH1106_regValues[i++];
		uint8_t len = SH1106_regValues[i++];
		status += OLED_WriteCmd(ctx, r);
		for (uint8_t d = 0; d < len; d++) {
			uint8_t x = SH1106_regValues[i++];
			status += OLED_WriteCmd(ctx, x);
		}
	}
#endif
	/* Clear screen */
	OLED_FillScreen(ctx, Black);
	/* Update screen */
	status += OLED_UpdateScreen(ctx);
	OLED_SetTextSize(ctx, 0);
	OLED_SetTextColor(ctx, White);
	return (status == ESP_OK) ? ESP_OK : ESP_FAIL;
}

/* ==================================================
 * ========= End of I2C Specific functions ==========
 * ================================================== */

/* ==================================================
 * ======== Interface independent functions =========
 * ================================================== */

esp_err_t OLED_UpdateScreen(oled_context_t *ctx) {
#if defined(SSD1306)
#if defined(USE_HORZMODE) || defined(USE_VERTMODE)
	return OLED_WriteData(ctx, ctx->display_buf, ctx->m_width * ctx->m_height / 8);
#elif defined(USE_PAGEMODE)
	esp_err_t status = ESP_OK;
	for (uint8_t i = 0; i < 8; i++) {
		status += OLED_WriteCmd(ctx, SSD1306_SETCOLUMNSTARTLOW | 0x00);
		status += OLED_WriteCmd(ctx, SSD1306_SETCOLUMNSTARTHIGH | 0x00);
		status += OLED_WriteCmd(ctx, SSD1306_SETPAGESTART | i);
		status += OLED_WriteData(ctx, &ctx->display_buf[ctx->m_width * i], ctx->m_width);
	}
	return (status == ESP_OK) ? ESP_OK : ESP_FAIL;
#endif
#elif defined(SH1106)
	esp_err_t status = ESP_OK;
	for (uint8_t i = 0; i < 8; i++) {
		// SH1106 ram has 132px, OLED matrix is 128px, they are center aligned - so image has to be shifted 2px!!
		status += OLED_WriteCmd(ctx, SH1106_SETCOLUMNSTARTLOW | 0x02);
		status += OLED_WriteCmd(ctx, SH1106_SETCOLUMNSTARTHIGH | 0x00);
		status += OLED_WriteCmd(ctx, SH1106_SETPAGESTART | i);
		status += OLED_WriteData(ctx, &ctx->display_buf[ctx->m_width * i], ctx->m_width);
	}
	return (status == ESP_OK) ? ESP_OK : ESP_FAIL;
#endif
}

void OLED_DrawPixel(oled_context_t *ctx, uint8_t x, uint8_t y, OLED_Color_t color) {
	if (x >= ctx->m_width || y >= ctx->m_height) {
		ESP_LOGW("oled-driver", "OOB DrawPixel (%d,%d)", x, y);
		return;  // Error
	}
	/* Check if pixels are inverted */
#if defined(OLED_COLORZONE)
	switch (ctx->m_inverted) {
	case Zone_None:
		break;
	case Zone_Color1:
		if (x * y < OLED_COLORZONE_EDGE) {
			color = (OLED_Color_t)!color;
		}
		break;
	case Zone_Color2:
		if (x * y >= OLED_COLORZONE_EDGE) {
			color = (OLED_Color_t)!color;
		}
		break;
	case Zone_Both:
		color = (OLED_Color_t)!color;
		break;
	}
#else
	if (ctx->m_inverted) {
		color = (OLED_Color_t)!color;
	}
#endif

	/* Set color */
#if defined (USE_LOOKUP)
	if (color == White) {
		ctx->display_buf[lookup_x[x] + lookup_y[0][y]] |= lookup_y[1][y];
	} else {
		ctx->display_buf[lookup_x[x] + lookup_y[0][y]] &= lookup_y[2][y];
	}
#else
#if defined(USE_HORZMODE) || defined(USE_PAGEMODE)
	if (color == White) {
		ctx->display_buf[x + (y >> 3) * ctx->m_width] |= 1 << (y & 0x7);
	} else {
		ctx->display_buf[x + (y >> 3) * ctx->m_width] &= ~(1 << (y & 0x7));
	}
#elif defined(USE_VERTMODE)
	if (color == White) {
		ctx->display_buf[((x * ctx->m_height) >> 3) + (y >> 3)] |= 1 << (y & 0x7);
	} else {
		ctx->display_buf[((x * ctx->m_height) >> 3) + (y >> 3)] &= ~(1 << (y & 0x7));
	}
#endif
#endif
}

void OLED_FillScreen(oled_context_t *ctx, OLED_Color_t color) {
	/* Set memory */
	memset(ctx->display_buf, (color == Black) ? 0x00 : 0xFF, ctx->m_width * ctx->m_height / 8);
}

void OLED_DrawBuffer(oled_context_t *ctx, int16_t x, int16_t y, int16_t w, int16_t h, uint8_t *buf, bool flip_x, bool flip_y) {
	
	for (uint8_t i = 0; i < w; i++) {
		for (uint8_t j = 0; j < h; j++) {
#if defined(USE_HORZMODE) || defined(USE_PAGEMODE)
			OLED_DrawPixel(ctx, x + (flip_x ? w - i - 1 : i), y + (flip_y ? h - j - 1 : j), (buf[i + (j >> 3) * w] >> (j & 0x7)) & 0x1);
#elif defined(USE_VERTMODE)
			OLED_DrawPixel(ctx, x + (flip_x ? w - i - 1 : i), y + (flip_y ? h - j - 1 : j), (buf[(i * h) / 8 + (j >> 3)] >> (j & 0x7)) & 0x1);
#endif
		}
	}
}

#if defined(OLED_COLORZONE)
void OLED_ToggleInvert(OLED_Zone_t zone) {
	uint16_t start = 0, end = 0;
	/* Toggle invert */
	ctx->m_inverted ^= zone;
	/* Do memory toggle */
	switch (zone) {
	case Zone_None:
		return;
	case Zone_Color1:
		start = 0;
		end = OLED_COLORZONE_EDGE;
		break;
	case Zone_Color2:
		start = OLED_COLORZONE_EDGE;
		end = ((ctx->m_width * ctx->m_height) / 8);
		break;
	case Zone_Both:
		start = 0;
		end = ((ctx->m_width * ctx->m_height) / 8);
		break;
	}
	for (uint16_t i = start; i < end; i++) {
		ctx->display_buf[i] = ~ctx->display_buf[i];
	}
}
#else
void OLED_ToggleInvert(oled_context_t *ctx) {
	/* Toggle invert */
	ctx->m_inverted = !ctx->m_inverted;
	/* Do memory toggle */
	for (uint16_t i = 0; i < ((ctx->m_width * ctx->m_height) / 8); i++) {
		ctx->display_buf[i] = ~ctx->display_buf[i];
	}
}
#endif

static void OLED_DrawFastHLine(oled_context_t *ctx, uint16_t x, uint16_t y, uint16_t w, OLED_Color_t color) {
	uint16_t x1 = x + w - 1;
	for (; x <= x1; x++) {
		OLED_DrawPixel(ctx, x, y, color);
	}
}

static void OLED_DrawFastVLine(oled_context_t *ctx, uint16_t x, uint16_t y, uint16_t h, OLED_Color_t color) {
	uint16_t y1 = y + h - 1;
	for (; y <= y1; y++) {
		OLED_DrawPixel(ctx, x, y, color);
	}
}

void OLED_DrawLine(oled_context_t *ctx, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, OLED_Color_t color) {
	/* Check for overflow */
	if (x0 >= ctx->m_width) {
		x0 = ctx->m_width - 1;
	}
	if (x1 >= ctx->m_width) {
		x1 = ctx->m_width - 1;
	}
	if (y0 >= ctx->m_height) {
		y0 = ctx->m_height - 1;
	}
	if (y1 >= ctx->m_height) {
		y1 = ctx->m_height - 1;
	}
	/* Check if Fast method can be applied */
	if (x0 == x1) {
		if (y0 > y1) {
			swap(y0, y1);
		}
		OLED_DrawFastVLine(ctx, x0, y0, y1 - y0 + 1, color);
	} else if (y0 == y1) {
		if (x0 > x1) {
			swap(x0, x1);
		}
		OLED_DrawFastHLine(ctx, x0, y0, x1 - x0 + 1, color);
	} else {
		// Bresenham's algorithm - thx wikpedia
		int16_t steep = abs(y1 - y0) > abs(x1 - x0);
		if (steep) {
			swap(x0, y0);
			swap(x1, y1);
		}

		if (x0 > x1) {
			swap(x0, x1);
			swap(y0, y1);
		}

		int16_t dx, dy;
		dx = x1 - x0;
		dy = abs(y1 - y0);

		int16_t err = dx / 2;
		int16_t ystep;

		if (y0 < y1) {
			ystep = 1;
		} else {
			ystep = -1;
		}

		for (; x0 <= x1; x0++) {
			if (steep) {
				OLED_DrawPixel(ctx, y0, x0, color);
			} else {
				OLED_DrawPixel(ctx, x0, y0, color);
			}
			err -= dy;
			if (err < 0) {
				y0 += ystep;
				err += dx;
			}
		}
	}
}

void OLED_DrawRect(oled_context_t *ctx, uint16_t x, uint16_t y, uint16_t w, uint16_t h, OLED_Color_t color) {
	/* Check input parameters */
	if (x >= ctx->m_width || y >= ctx->m_height) {
		/* Return error */
		return;
	}
	/* Check width and height */
	if ((x + w) >= ctx->m_width) {
		w = ctx->m_width - x;
	}
	if ((y + h) >= ctx->m_height) {
		h = ctx->m_height - y;
	}
	/* Draw 4 lines */
	OLED_DrawFastHLine(ctx, x, y, w, color);                 /* Top line */
	OLED_DrawFastHLine(ctx, x, y + h - 1, w, color);         /* Bottom line */
	OLED_DrawFastVLine(ctx, x, y, h, color);                 /* Left line */
	OLED_DrawFastVLine(ctx, x + w - 1, y, h, color);         /* Right line */
}

void OLED_FillRect(oled_context_t *ctx, uint16_t x, uint16_t y, uint16_t w, uint16_t h, OLED_Color_t color) {
	/* Check input parameters */
	if (x >= ctx->m_width || y >= ctx->m_height) {
		/* Return error */
		return;
	}
	/* Check width and height */
	if ((x + w) >= ctx->m_width) {
		w = ctx->m_width - x;
	}
	if ((y + h) >= ctx->m_height) {
		h = ctx->m_height - y;
	}
	/* Draw lines */
	for (uint8_t i = 0; i < h; i++) {
		/* Draw lines */
		OLED_DrawFastHLine(ctx, x, y + i, w, color);
	}
}

void OLED_DrawCircle(oled_context_t *ctx, int16_t x0, int16_t y0, int16_t r, OLED_Color_t color) {
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	OLED_DrawPixel(ctx, x0, y0 + r, color);
	OLED_DrawPixel(ctx, x0, y0 - r, color);
	OLED_DrawPixel(ctx, x0 + r, y0, color);
	OLED_DrawPixel(ctx, x0 - r, y0, color);

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		OLED_DrawPixel(ctx, x0 + x, y0 + y, color);
		OLED_DrawPixel(ctx, x0 - x, y0 + y, color);
		OLED_DrawPixel(ctx, x0 + x, y0 - y, color);
		OLED_DrawPixel(ctx, x0 - x, y0 - y, color);
		OLED_DrawPixel(ctx, x0 + y, y0 + x, color);
		OLED_DrawPixel(ctx, x0 - y, y0 + x, color);
		OLED_DrawPixel(ctx, x0 + y, y0 - x, color);
		OLED_DrawPixel(ctx, x0 - y, y0 - x, color);
	}
}

void OLED_DrawCircleHelper(oled_context_t *ctx, int16_t x0, int16_t y0, int16_t r, uint8_t cornername, OLED_Color_t color) {
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;
		if (cornername & 0x4) {
			OLED_DrawPixel(ctx, x0 + x, y0 + y, color);
			OLED_DrawPixel(ctx, x0 + y, y0 + x, color);
		}
		if (cornername & 0x2) {
			OLED_DrawPixel(ctx, x0 + x, y0 - y, color);
			OLED_DrawPixel(ctx, x0 + y, y0 - x, color);
		}
		if (cornername & 0x8) {
			OLED_DrawPixel(ctx, x0 - y, y0 + x, color);
			OLED_DrawPixel(ctx, x0 - x, y0 + y, color);
		}
		if (cornername & 0x1) {
			OLED_DrawPixel(ctx, x0 - y, y0 - x, color);
			OLED_DrawPixel(ctx, x0 - x, y0 - y, color);
		}
	}
}

void OLED_FillCircle(oled_context_t *ctx, int16_t x0, int16_t y0, int16_t r, OLED_Color_t color) {
	OLED_DrawLine(ctx, x0, y0 - r, x0, y0 + r, color);
	OLED_FillCircleHelper(ctx, x0, y0, r, 3, 0, color);
}

void OLED_FillCircleHelper(oled_context_t *ctx, uint16_t x0, uint16_t y0, uint16_t r, uint8_t cornername, int16_t delta, OLED_Color_t color) {
	int16_t f = 1 - r;
	int16_t ddF_x = 1;
	int16_t ddF_y = -2 * r;
	int16_t x = 0;
	int16_t y = r;

	while (x < y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f += ddF_y;
		}
		x++;
		ddF_x += 2;
		f += ddF_x;

		if (cornername & 0x1) {
			OLED_DrawLine(ctx, x0 + x, y0 - y, x0 + x, y0 + y + delta, color);
			OLED_DrawLine(ctx, x0 + y, y0 - x, x0 + y, y0 + x + delta, color);
		}
		if (cornername & 0x2) {
			OLED_DrawLine(ctx, x0 - x, y0 - y, x0 - x, y0 + y + delta, color);
			OLED_DrawLine(ctx, x0 - y, y0 - x, x0 - y, y0 + x + delta, color);
		}
	}
}

void OLED_DrawTriangle(oled_context_t *ctx, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, OLED_Color_t color) {
	/* Draw lines */
	OLED_DrawLine(ctx, x0, y0, x1, y1, color);
	OLED_DrawLine(ctx, x1, y1, x2, y2, color);
	OLED_DrawLine(ctx, x2, y2, x0, y0, color);
}

void OLED_FillTriangle(oled_context_t *ctx, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, OLED_Color_t color) {
	int16_t a, b, y, last;

	// Sort coordinates by Y order (y2 >= y1 >= y0)
	if (y0 > y1) {
		swap(y0, y1);
		swap(x0, x1);
	}
	if (y1 > y2) {
		swap(y2, y1);
		swap(x2, x1);
	}
	if (y0 > y1) {
		swap(y0, y1);
		swap(x0, x1);
	}

	if (y0 == y2) {  // Handle awkward all-on-same-line case as its own thing
		a = b = x0;
		if (x1 < a)
			a = x1;
		else if (x1 > b)
			b = x1;
		if (x2 < a)
			a = x2;
		else if (x2 > b)
			b = x2;
		OLED_DrawLine(ctx, a, y0, b, y0, color);
		return;
	}

	int16_t dx01 = x1 - x0, dy01 = y1 - y0, dx02 = x2 - x0, dy02 = y2 - y0, dx12 = x2 - x1, dy12 = y2 - y1;
	int32_t sa = 0, sb = 0;

	// For upper part of triangle, find scanline crossings for segments
	// 0-1 and 0-2.  If y1=y2 (flat-bottomed triangle), the scanline y1
	// is included here (and second loop will be skipped, avoiding a /0
	// error there), otherwise scanline y1 is skipped here and handled
	// in the second loop...which also avoids a /0 error here if y0=y1
	// (flat-topped triangle).
	if (y1 == y2)
		last = y1;  // Include y1 scanline
	else
		last = y1 - 1;  // Skip it

	for (y = y0; y <= last; y++) {
		a = x0 + sa / dy01;
		b = x0 + sb / dy02;
		sa += dx01;
		sb += dx02;
		/* longhand:
		 a = x0 + (x1 - x0) * (y - y0) / (y1 - y0);
		 b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		 */
		if (a > b)
			swap(a, b);
		OLED_DrawLine(ctx, a, y, b, y, color);
	}

	// For lower part of triangle, find scanline crossings for segments
	// 0-2 and 1-2.  This loop is skipped if y1=y2.
	sa = dx12 * (y - y1);
	sb = dx02 * (y - y0);
	for (; y <= y2; y++) {
		a = x1 + sa / dy12;
		b = x0 + sb / dy02;
		sa += dx12;
		sb += dx02;
		/* longhand:
		 a = x1 + (x2 - x1) * (y - y1) / (y2 - y1);
		 b = x0 + (x2 - x0) * (y - y0) / (y2 - y0);
		 */
		if (a > b)
			swap(a, b);
		OLED_DrawLine(ctx, a, y, b, y, color);
	}
}
void OLED_DrawRoundRect(oled_context_t *ctx, int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, OLED_Color_t color) {
	// smarter version
	OLED_DrawLine(ctx, x + r, y, x + w - r, y, color);                  // Top
	OLED_DrawLine(ctx, x + r, y + h - 1, x + w - r, y + h - 1, color);  // Bottom
	OLED_DrawLine(ctx, x, y + r, x, y + h - r, color);                  // Left
	OLED_DrawLine(ctx, x + w - 1, y + r, x + w - 1, y + h - r, color);  // Right
	// draw four corners
	OLED_DrawCircleHelper(ctx, x + r, y + r, r, 1, color);
	OLED_DrawCircleHelper(ctx, x + w - r - 1, y + r, r, 2, color);
	OLED_DrawCircleHelper(ctx, x + w - r - 1, y + h - r - 1, r, 4, color);
	OLED_DrawCircleHelper(ctx, x + r, y + h - r - 1, r, 8, color);
}
void OLED_FillRoundRect(oled_context_t *ctx, int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, OLED_Color_t color) {
	// smarter version
	OLED_FillRect(ctx, x + r, y, w - 2 * r, h, color);
	// draw four corners
	OLED_FillCircleHelper(ctx, x + w - r - 1, y + r, r, 1, h - 2 * r - 1, color);
	OLED_FillCircleHelper(ctx, x + r, y + r, r, 2, h - 2 * r - 1, color);
}

void OLED_DrawChar(oled_context_t *ctx, int16_t x, int16_t y, uint8_t c, OLED_Color_t color, uint8_t fontindex) {
	uint16_t height, width, bytes;
	uint8_t offset;
	uint32_t charindex = 0;
	uint8_t *pchar;
	uint32_t line = 0;

	height = fonts[fontindex]->Height;
	width = fonts[fontindex]->Width;

	if ((x >= ctx->m_width) ||    // Clip right
	    (y >= ctx->m_height) ||   // Clip bottom
	    ((x + width - 1) < 0) ||  // Clip left
	    ((y + height - 1) < 0))   // Clip top
		return;

	bytes = (width + 7) / 8;
	if (c < ' ') c = ' ';
#ifndef USE_CP1251
	else if (c > '~')
		c = ' ';
#endif
	charindex = (c - ' ') * height * bytes;
	offset = 8 * bytes - width;

	for (uint32_t i = 0; i < height; i++) {
		pchar = ((uint8_t *)&fonts[fontindex]->table[charindex] + (width + 7) / 8 * i);
		switch (bytes) {
		case 1:
			line = pchar[0];
			break;
		case 2:
			line = (pchar[0] << 8) | pchar[1];
			break;
		case 3:
		default:
			line = (pchar[0] << 16) | (pchar[1] << 8) | pchar[2];
			break;
		}
		for (uint32_t j = 0; j < width; j++) {
			if (line & (1 << (width - j + offset - 1))) {
				OLED_DrawPixel(ctx, (x + j), y, color);
			} else {
				OLED_DrawPixel(ctx, (x + j), y, !color);
			}
		}
		y++;
	}
}
void OLED_Printf(oled_context_t *ctx, const char *fmt, ...) {
	static char buf[256];
	char *p;
	va_list lst;

	va_start(lst, fmt);
	vsprintf(buf, fmt, lst);
	va_end(lst);

	volatile uint16_t height, width;
	height = fonts[ctx->m_font]->Height;
	width = fonts[ctx->m_font]->Width;

	p = buf;
	while (*p) {
		if (*p == '\n') {
			ctx->m_cursor_y += height;
			ctx->m_cursor_x = 0;
		} else if (*p == '\r') {
			ctx->m_cursor_x = 0;
		} else if (*p == '\t') {
			ctx->m_cursor_x += width * 4;
		} else {
			if (ctx->m_cursor_x == 0) {
				OLED_FillRect(ctx, 0, ctx->m_cursor_y, ctx->m_width - 1, ctx->m_cursor_y + height - 1, !ctx->m_textcolor);
			}
			if (ctx->m_cursor_y >= (ctx->m_height - height)) {
				ctx->m_cursor_y = 0;
				OLED_FillScreen(ctx, !ctx->m_textcolor);
			}
			OLED_DrawChar(ctx, ctx->m_cursor_x, ctx->m_cursor_y, *p, ctx->m_textcolor, ctx->m_font);
			ctx->m_cursor_x += width;
			if (ctx->m_wrap && (ctx->m_cursor_x > (ctx->m_width - width))) {
				ctx->m_cursor_y += height;
				ctx->m_cursor_x = 0;
			}
		}
		p++;
	}
}

void OLED_SetCursor(oled_context_t *ctx, uint16_t x, uint16_t y) {
	/* Set write pointers */
	ctx->m_cursor_x = x;
	ctx->m_cursor_y = y;
}

void OLED_SetTextSize(oled_context_t *ctx, uint8_t s) {
	if (s >= fontsNum) {
		ctx->m_font = fontsNum - 1;
	} else {
		ctx->m_font = s;
	}
}

void OLED_SetTextColor(oled_context_t *ctx, OLED_Color_t c) {
	ctx->m_textcolor = c;
}

void OLED_SetTextWrap(oled_context_t *ctx, uint8_t w) {
	ctx->m_wrap = w;
}

int16_t OLED_GetCursorX(oled_context_t *ctx) {
	return ctx->m_cursor_x;
}

int16_t OLED_GetCursorY(oled_context_t *ctx) {
	return ctx->m_cursor_y;
}

void OLED_DisplayOn(oled_context_t *ctx) {
#if defined(SSD1306)
	OLED_WriteCmd(ctx, SSD1306_SETCHARGEPUMP);
	OLED_WriteCmd(ctx, SSD1306_CHARGEPUMP_ON);
	OLED_WriteCmd(ctx, SSD1306_SETDISPLAY_ON);
#elif defined(SH1106)
	OLED_WriteCmd(ctx, SH1106_SETDCDC);
	OLED_WriteCmd(ctx, SH1106_DCDC_ON);
	OLED_WriteCmd(ctx, SH1106_SETDISPLAY_ON);
#endif
}

void OLED_DisplayOff(oled_context_t *ctx) {
#if defined(SSD1306)
	OLED_WriteCmd(ctx, SSD1306_SETDISPLAY_OFF);
	OLED_WriteCmd(ctx, SSD1306_SETCHARGEPUMP);
	OLED_WriteCmd(ctx, SSD1306_CHARGEPUMP_OFF);
#elif defined(SH1106)
	OLED_WriteCmd(ctx, SH1106_SETDISPLAY_OFF);
	OLED_WriteCmd(ctx, SH1106_SETDCDC);
	OLED_WriteCmd(ctx, SH1106_DCDC_OFF);
#endif
}

#if defined(SSD1306)
void OLED_ScrollStartHorzRight(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step) {
	OLED_WriteCmd(ctx, SSD1306_SCROLL_SET_VERT_AREA);
	OLED_WriteCmd(ctx, startrow);
	OLED_WriteCmd(ctx, stoprow);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_HORZ_RIGHT);
	OLED_WriteCmd(ctx, 0x00);
	OLED_WriteCmd(ctx, startrow / 8);
	OLED_WriteCmd(ctx, step);
	OLED_WriteCmd(ctx, stoprow / 8);
	OLED_WriteCmd(ctx, 0x00);
	OLED_WriteCmd(ctx, 0xFF);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_ACTIVATE);
}

void OLED_ScrollStartHorzLeft(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step) {
	OLED_WriteCmd(ctx, SSD1306_SCROLL_SET_VERT_AREA);
	OLED_WriteCmd(ctx, startrow);
	OLED_WriteCmd(ctx, stoprow);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_HORZ_LEFT);
	OLED_WriteCmd(ctx, 0x00);
	OLED_WriteCmd(ctx, startrow / 8);
	OLED_WriteCmd(ctx, step);
	OLED_WriteCmd(ctx, stoprow / 8);
	OLED_WriteCmd(ctx, 0x00);
	OLED_WriteCmd(ctx, 0xFF);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_ACTIVATE);
}

void OLED_ScrollStartDiagRight(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step, uint8_t vertoffset) {
	OLED_WriteCmd(ctx, SSD1306_SCROLL_SET_VERT_AREA);
	OLED_WriteCmd(ctx, startrow);
	OLED_WriteCmd(ctx, stoprow);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_DIAG_RIGHT);
	OLED_WriteCmd(ctx, 0x00);
	OLED_WriteCmd(ctx, startrow / 8);
	OLED_WriteCmd(ctx, step);
	OLED_WriteCmd(ctx, stoprow / 8);
	OLED_WriteCmd(ctx, vertoffset);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_ACTIVATE);
}

void OLED_ScrollStartDiagLeft(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step, uint8_t vertoffset) {
	OLED_WriteCmd(ctx, SSD1306_SCROLL_SET_VERT_AREA);
	OLED_WriteCmd(ctx, startrow);
	OLED_WriteCmd(ctx, stoprow);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_DIAG_LEFT);
	OLED_WriteCmd(ctx, 0x00);
	OLED_WriteCmd(ctx, startrow / 8);
	OLED_WriteCmd(ctx, step);
	OLED_WriteCmd(ctx, stoprow / 8);
	OLED_WriteCmd(ctx, vertoffset);
	OLED_WriteCmd(ctx, SSD1306_SCROLL_ACTIVATE);
}

void OLED_ScrollStop(oled_context_t *ctx) {
	OLED_WriteCmd(ctx, SSD1306_SCROLL_DEACTIVATE);
}

void OLED_FadeStart(oled_context_t *ctx, uint8_t mode, uint8_t step) {
	OLED_WriteCmd(ctx, SSD1306_SETFADEOUTANDBLINK);
	OLED_WriteCmd(ctx, mode | step);
}

void OLED_FadeStop(oled_context_t *ctx) {
	OLED_WriteCmd(ctx, SSD1306_SETFADEOUTANDBLINK);
	OLED_WriteCmd(ctx, SSD1306_FADE_OFF);
}

#if OLEDHEIGHT > 32
void OLED_SetZoomIn(oled_context_t *ctx, uint8_t enable) {
	OLED_WriteCmd(ctx, SSD1306_SETZOOMIN);
	OLED_WriteCmd(ctx, enable);
}
#endif
#endif
