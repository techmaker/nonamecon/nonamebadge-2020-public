/**
 *	TechMaker
 *	https://techmaker.ua
 *
 *	STM32 LCD OLED Library for 0.96" / 1.3" displays using I2C/SPI
 *	based on Adafruit GFX & Adafruit TFT LCD libraries
 *	16 Aug 2018 by Alexander Olenyev <sasha@techmaker.ua>
 *
 *	Changelog:
 *		- v1.2 added SPI interface & return status codes
 *		- v1.1 added Courier New font family with Cyrillic (CP1251), created using TheDotFactory font generator
 *		- v1.0 added support for SSD1306 and SH1106 chips
 */

#pragma once

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include "registers.h"
#include "Fonts/fonts.h"
#include "i2c_drv.h"
#include "driver/gpio.h"
#include <stdio.h>

// Please uncomment one of the lines to select your OLED chip
#define SSD1306
// #define SH1106

#if defined (SSD1306)
#define USE_HORZMODE
// #define USE_VERTMODE
// #define USE_PAGEMODE
#elif defined (SH1106)
#define USE_PAGEMODE
#else
#error Please select your OLED chip in oled.h, lines 31-32
#endif

/* Use lookup table for pixel drawing (+ 1.2kB RAM for 128x64 display) */
#define USE_LOOKUP

/* OLED screen dimensions */
#define OLEDWIDTH			128
#define OLEDHEIGHT			64

/* Separate color zone toggling of bi-colored displays */
//#define OLED_COLORZONE

/* Available OLED addresses */
#define OLED_I2C_ADDR_1		0x3C	// marked 0x78 on the OLED
#define OLED_I2C_ADDR_2		0x3D	// marked 0x7A on the OLED

typedef struct {
	i2c_drv_t *i2c_drv;
	uint8_t i2c_addr;
	uint8_t *display_buf;
	int16_t m_width;
	int16_t m_height;
	int16_t m_cursor_x;
	int16_t m_cursor_y;
	uint16_t m_textcolor;
	uint8_t m_font;
	uint8_t m_inverted;
	uint8_t m_wrap;
#if defined(OLED_COLORZONE)
	OLED_Zone_t m_inverted;
#endif
} oled_context_t;

typedef enum {
	Black = 0x00,	/*!< Black color, no pixel */
	White = 0x01	/*!< Pixel is set. Color depends on LCD */
} OLED_Color_t;

#if defined(OLED_COLORZONE)
#define OLED_COLORZONE_LINES	16		/* How many lines are in color zone 1 */
#define OLED_COLORZONE_EDGE 	(OLED_COLORZONE_LINES * OLEDWIDTH / 8)	
typedef enum {
	Zone_None = 0x00,
	Zone_Color1 = 0x01,
	Zone_Color2 = 0x02,
	Zone_Both = Zone_Color1 | Zone_Color2
} OLED_Zone_t;
#endif

#define swap(a, b)			do {\
								int16_t t = a;\
								a = b;\
								b = t;\
							} while(0)

esp_err_t OLED_Init(oled_context_t *ctx);
void OLED_DrawPixel(oled_context_t *ctx, uint8_t x, uint8_t y, OLED_Color_t color);
esp_err_t OLED_UpdateScreen(oled_context_t *ctx);
void OLED_FillScreen(oled_context_t *ctx, OLED_Color_t Color);
void OLED_DrawBuffer(oled_context_t *ctx, int16_t x, int16_t y, int16_t w, int16_t h, uint8_t *buf, bool flip_x, bool flip_y);

#if defined(OLED_COLORZONE)
void OLED_ToggleInvert(OLED_Zone_t zone);
#else
void OLED_ToggleInvert(oled_context_t *ctx);
#endif

void OLED_DrawLine(oled_context_t *ctx, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, OLED_Color_t color);
void OLED_DrawRect(oled_context_t *ctx, uint16_t x, uint16_t y, uint16_t w, uint16_t h, OLED_Color_t color);
void OLED_FillRect(oled_context_t *ctx, uint16_t x, uint16_t y, uint16_t w, uint16_t h, OLED_Color_t color);

void OLED_DrawCircle(oled_context_t *ctx, int16_t x0, int16_t y0, int16_t r, OLED_Color_t color);
void OLED_DrawCircleHelper(oled_context_t *ctx, int16_t x0, int16_t y0, int16_t r, uint8_t cornername, OLED_Color_t color);
void OLED_FillCircle(oled_context_t *ctx, int16_t x0, int16_t y0, int16_t r, OLED_Color_t color);
void OLED_FillCircleHelper(oled_context_t *ctx, uint16_t x0, uint16_t y0, uint16_t r, uint8_t cornername, int16_t delta, OLED_Color_t color);
void OLED_DrawTriangle(oled_context_t *ctx, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, OLED_Color_t color);
void OLED_FillTriangle(oled_context_t *ctx, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, OLED_Color_t color);
void OLED_DrawRoundRect(oled_context_t *ctx, int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, OLED_Color_t color);
void OLED_FillRoundRect(oled_context_t *ctx, int16_t x, int16_t y, int16_t w, int16_t h, int16_t r, OLED_Color_t color);

void OLED_DrawChar(oled_context_t *ctx, int16_t x, int16_t y, uint8_t c, OLED_Color_t color, uint8_t fontIndex);
void OLED_Printf(oled_context_t *ctx, const char *fmt, ...);

void OLED_SetCursor(oled_context_t *ctx, uint16_t x, uint16_t y);
void OLED_SetTextSize(oled_context_t *ctx, uint8_t s);
void OLED_SetTextColor(oled_context_t *ctx, OLED_Color_t c);
void OLED_SetTextWrap(oled_context_t *ctx, uint8_t w);
int16_t OLED_GetCursorX(oled_context_t *ctx);
int16_t OLED_GetCursorY(oled_context_t *ctx);

void OLED_DisplayOn(oled_context_t *ctx);
void OLED_DisplayOff(oled_context_t *ctx);

#if defined(SSD1306)
void OLED_ScrollStartHorzRight(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step);
void OLED_ScrollStartHorzLeft(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step);
void OLED_ScrollStartDiagRight(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step, uint8_t vertoffset);
void OLED_ScrollStartDiagLeft(oled_context_t *ctx, uint8_t startrow, uint8_t stoprow, uint8_t step, uint8_t vertoffset);
void OLED_ScrollStop(oled_context_t *ctx);
void OLED_FadeStart(oled_context_t *ctx, uint8_t mode, uint8_t step);
void OLED_FadeStop(oled_context_t *ctx);
#if OLEDHEIGHT > 32
void OLED_SetZoomIn(oled_context_t *ctx, uint8_t enable);
#endif
#endif
